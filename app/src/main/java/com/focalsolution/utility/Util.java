package com.focalsolution.utility;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.util.TypedValue;
import android.view.inputmethod.InputMethodManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    public static final String APP_IMAGE_FOLDER = "ticketproject";
    static final int timeoutConnection = 8000, timeoutSocket = 8500, timeoutThread = 9000;
	//convert Pixels to Dips.
	public static float convertPixtoDip(int pixel, Resources r)
	{
        /*
        float scale = Setting.getDensity();
        return (int) (pixel * scale + 0.5f);
        */
        float dp = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PX, pixel, r.getDisplayMetrics());
        return dp;
	}
	//convert Dip to Pixels.
	public static float convertDiptoPix(int dip, Resources r)
	{
        /*
        float scale = Setting.getDensity();
        return (int)((dip - 0.5f)/scale);
        */
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, r.getDisplayMetrics());
        return px;
	 }
	public static int toInterger(String value, int defaultValue)
	{
		int d = defaultValue;

		try {
			d = Integer.parseInt(value);
		} catch (Exception e) {
			// TODO: handle exception
			d = defaultValue;
		}

		return d;
	}

    public static double toDouble(String value, double defaultValue)
    {
        double d = defaultValue;

        try {
            d = Double.parseDouble(value);
        } catch (Exception e) {
            // TODO: handle exception
            d = defaultValue;
        }

        return d;
    }

	public static Location getBestLocation(LocationManager lm)
    {
    	Location bestLocation = null;
    	boolean providerEnabled = false;

    	for (String str : lm.getAllProviders()) {

    		Log.i("BR", "get location " + str);

    		try
    		{
    			providerEnabled=lm.isProviderEnabled(str);
    		}
    		catch(Exception ex)
    		{
    			providerEnabled=false;
    		}

    		if(providerEnabled)
    		{

    			if (bestLocation == null)
    			{
    				bestLocation = lm.getLastKnownLocation(str);
    				Log.i("BR", "get first best " + bestLocation);
    				continue;
    			}

	    		Location newlocation = lm.getLastKnownLocation(str);
	    		Log.i("BR", "get best " + bestLocation);
	    		Log.i("BR", "get new " + newlocation);

	    		if(newlocation!=null)
	    		{
		    		if(isBetterLocation(newlocation,bestLocation))
		    			bestLocation = newlocation;
	    		}
    		}
        }
    	Log.i("BR", "Return location " + bestLocation);
    	return bestLocation;
    }

    public static void showSoftKeyboard(Activity activity)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null){
            imm.toggleSoftInput(0, InputMethodManager.SHOW_IMPLICIT);
        }
    }
    public static void hideSoftKeyboard(Activity activity)
    {
        InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm != null){
            imm.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

	private static final int FIVE_SEC = 1000 * 5;
	/** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
   protected static boolean isBetterLocation(Location location, Location currentBestLocation) {
       if (currentBestLocation == null) {
           // A new location is always better than no location
           return true;
       }

       if(location.equals(currentBestLocation))
       	return false;

       // Check whether the new location fix is newer or older
       long timeDelta = location.getTime() - currentBestLocation.getTime();
       boolean isSignificantlyNewer = timeDelta > FIVE_SEC;
       boolean isSignificantlyOlder = timeDelta < -FIVE_SEC;
       boolean isNewer = timeDelta > 0;

       // If it's been more than two minutes since the current location, use the new location
       // because the user has likely moved
       if (isSignificantlyNewer) {
           return true;
       // If the new location is more than two minutes older, it must be worse
       } else if (isSignificantlyOlder) {
           return false;
       }

       // Check whether the new location fix is more or less accurate
       int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
       boolean isLessAccurate = accuracyDelta > 0;
       boolean isMoreAccurate = accuracyDelta < 0;
       boolean isSignificantlyLessAccurate = accuracyDelta > 200;

       // Check if the old and new location are from the same provider
       boolean isFromSameProvider = isSameProvider(location.getProvider(),
               currentBestLocation.getProvider());

       // Determine location quality using a combination of timeliness and accuracy
       if (isMoreAccurate) {
           return true;
       } else if (isNewer && !isLessAccurate) {
           return true;
       } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
           return true;
       }
       return false;
   }

   /** Checks whether two providers are the same */
   private static boolean isSameProvider(String provider1, String provider2) {
       if (provider1 == null) {
         return provider2 == null;
       }
       return provider1.equals(provider2);
   }

	public static void dirChecker(String dir) {
	    File f = new File(dir);

	    if(!f.isDirectory()) {
	      f.mkdirs();
	    }
	  }
	public static boolean fileChecker(String path){
		try {
			File f = new File(path);
			return f.exists();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
			return false;
		}

	}
	public static void copyFile(InputStream in, File to) throws IOException
	{

	    OutputStream out = new FileOutputStream(to);

	    // Transfer bytes from in to out
	    byte[] buf = new byte[1024];
	    int len;
	    while ((len = in.read(buf)) > 0) {
	        out.write(buf, 0, len);
	    }
	    in.close();
	    out.close();
	}
	public static void DeleteDirRecursive(File fileOrDirectory) {
	    if (fileOrDirectory.isDirectory())
	        for (File child : fileOrDirectory.listFiles())
	        	DeleteDirRecursive(child);

	    fileOrDirectory.delete();
	}

	public static String getUserEmail(Context context) {
	    AccountManager accountManager = AccountManager.get(context);
	    Account account = getAccount(accountManager);

	    if (account == null) {
	      return null;
	    } else {
	      return account.name;
	    }
	  }
    public static boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }
    public static String getYoutubeVideoID(String youtubeUrl)
    {
        if(youtubeUrl.startsWith("http://youtu.be/"))
        {
            return youtubeUrl.replace("http://youtu.be/","");
        } else {
            if(youtubeUrl.startsWith("http://www.youtube.com/")) {
                youtubeUrl = youtubeUrl.replace("http://www.youtube.com/watch?", "");
                String[] queryString = youtubeUrl.split("&");
                String youtubeVideoID = "";
                for (String query : queryString) {
                    if (query.startsWith("v=")) {
                        youtubeVideoID = query.replace("v=", "");
                        break;
                    }
                }

                return youtubeVideoID;
            } else {
                youtubeUrl = youtubeUrl.replace("https://www.youtube.com/watch?", "");
                String[] queryString = youtubeUrl.split("&");
                String youtubeVideoID = "";
                for (String query : queryString) {
                    if (query.startsWith("v=")) {
                        youtubeVideoID = query.replace("v=", "");
                        break;
                    }
                }

                return youtubeVideoID;
            }
        }
    }

    public static String getYoutubeThumbnailImage(String youtubeVideoID, int image)
    {
        final String youtubeImageUrl = "http://i.ytimg.com/vi/";

        return youtubeImageUrl + youtubeVideoID + "/"+ image + ".jpg";
    }
    public static String getYoutubeThumbnailImage(String youtubeVideoID)
    {
        return getYoutubeThumbnailImage(youtubeVideoID, 0);
    }


	private static Account getAccount(AccountManager accountManager) {
	    Account[] accounts = accountManager.getAccountsByType("com.google");
	    Account account;
	    if (accounts.length > 0) {
	      account = accounts[0];
	    } else {
	      account = null;
	    }
	    return account;
	  }

	/**
     * Function to convert milliseconds time to
     * Timer Format
     * Hours:Minutes:Seconds
     * */
    public static String milliSecondsToTimer(long milliseconds){
        String finalTimerString = "";
        String secondsString = "";

        // Convert total duration into time
           int hours = (int)( milliseconds / (1000*60*60));
           int minutes = (int)(milliseconds % (1000*60*60)) / (1000*60);
           int seconds = (int) ((milliseconds % (1000*60*60)) % (1000*60) / 1000);
           // Add hours if there
           if(hours > 0){
               finalTimerString = hours + ":";
           }

           // Prepending 0 to seconds if it is one digit
           if(seconds < 10){
               secondsString = "0" + seconds;
           }else{
               secondsString = "" + seconds;}

           finalTimerString = finalTimerString + minutes + ":" + secondsString;

        // return timer string
        return finalTimerString;
    }

    /**
     * Function to get Progress percentage
     * @param currentDuration
     * @param totalDuration
     * */
    public static int getProgressPercentage(long currentDuration, long totalDuration){
        Double percentage = (double) 0;

        long currentSeconds = (int) (currentDuration / 1000);
        long totalSeconds = (int) (totalDuration / 1000);

        // calculating percentage
        percentage =(((double)currentSeconds)/totalSeconds)*100;

        // return percentage
        return percentage.intValue();
    }

    /**
     * Function to change progress to timer
     * @param progress -
     * @param totalDuration
     * returns current duration in milliseconds
     * */
    public static int progressToTimer(int progress, int totalDuration) {
        int currentDuration = 0;
        totalDuration = (int) (totalDuration / 1000);
        currentDuration = (int) ((((double)progress) / 100) * totalDuration);

        // return current duration in milliseconds
        return currentDuration * 1000;
    }

    public static double calDistance(double lat1, double lon1, double lat2, double lon2, char unit) {
    	  double theta = lon1 - lon2;
    	  double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
    	  dist = Math.acos(dist);
    	  dist = rad2deg(dist);
    	  dist = dist * 60 * 1.1515;
    	  if (unit == 'K') {
    	    dist = dist * 1.609344;
    	  } else if (unit == 'N') {
    	  	dist = dist * 0.8684;
    	    }
    	  return (dist);
    	}

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts decimal degrees to radians             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double deg2rad(double deg) {
    	  return (deg * Math.PI / 180.0);
    	}

    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    /*::  This function converts radians to decimal degrees             :*/
    /*:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::*/
    private static double rad2deg(double rad) {
    	  return (rad * 180 / Math.PI);
    	}

    public static boolean isOnline(Context c) {
        return isOnline(c, "http://www.creativejourneyapp.com/dasta/", "Dasta Login.");
    }

    public static boolean isOnline(Context c, String testUrl, String finder)
    {
        ConnectivityManager cm =
                (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return checkConnectionToServer(testUrl, finder);
        }
        return false;
    }
    static boolean flgLoad = false;
    private static boolean checkConnectionToServer(String url, String finder)
    {
        try {
            Log.i("ConnTest", "Start Test!");
            final HttpGet httpGet = new HttpGet(url);

            HttpParams httpParameters = new BasicHttpParams();
            // Set the timeout in milliseconds until a connection is established.
            // The default value is zero, that means the timeout is not used.

            HttpConnectionParams.setConnectionTimeout(httpParameters, timeoutConnection);
            // Set the default socket timeout (SO_TIMEOUT)
            // in milliseconds which is the timeout for waiting for data.

            HttpConnectionParams.setSoTimeout(httpParameters, timeoutSocket);

            DefaultHttpClient httpClient = new DefaultHttpClient(httpParameters);

            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        Thread.sleep(timeoutThread);
                        if(!flgLoad)
                        {
                            httpGet.abort();
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();

            HttpResponse response = httpClient.execute(httpGet);
            flgLoad = true;
            String web = convertStreamToString(response.getEntity().getContent());
            if(web.indexOf(finder)>0)
            {
                Log.i("ConnTest", "Find String OK");
                return true;
            } else {
                Log.i("ConnTest", "Find String OK");
                throw new Exception("Web Page Error");
            }
        } catch (Exception ex){
            ex.printStackTrace();
            return false;
        }
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append((line + "\n"));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    public static Bitmap getBitmapFromAsset(String strName, Activity activity)
    {
        AssetManager assetManager = activity.getAssets();
        InputStream istr = null;
        try {
            istr = assetManager.open(strName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Bitmap bitmap = BitmapFactory.decodeStream(istr);
        return bitmap;
    }

    public static File saveBitmapToGallary(Bitmap bmp) {

        OutputStream outStream = null;
        // String temp = null;
        File file = getOutputMediaFile(MEDIA_TYPE_IMAGE);

        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return file;
    }

    public static String getPathFromQuery(Uri uri, Activity activity) {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        //cursor.close();
        return s;
    }

    public static long getCurrentTimeStamp()
    {
        Calendar cal = Calendar.getInstance();
        return cal.getTimeInMillis()/1000;
    }

    public static long getCurrentTimeStamp(int day, int month, int year)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DATE, day);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        return cal.getTimeInMillis()/1000;
    }

    public static long getCurrentTimeStamp(int year, int month, int day, int hour, int min, int sec)
    {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, hour);
        cal.set(Calendar.MINUTE, min);
        cal.set(Calendar.SECOND, sec);
        cal.set(Calendar.DATE, day);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.YEAR, year);
        return cal.getTimeInMillis()/1000;
    }

    public static long addCurrentTimeStamp(int day, int month, int year)
    {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, day);
        cal.add(Calendar.MONTH, month);
        cal.add(Calendar.YEAR, year);
        return cal.getTimeInMillis()/1000;
    }

    public static String getDateString(long timeStamp, String format){

        try{
            DateFormat sdf = new SimpleDateFormat(format);
            Date netDate = (new Date(timeStamp*1000));
            return sdf.format(netDate);
        }
        catch(Exception ex){
            return "xx";
        }
    }

    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int MEDIA_TYPE_VIDEO = 2;

    /** Create a file Uri for saving an image or video */
    public static Uri getOutputMediaFileUri(int type){
        return Uri.fromFile(getOutputMediaFile(type));
    }

    /** Create a File for saving an image or video */
    public static File getOutputMediaFile(int type){
        // To be safe, you should check that the SDCard is mounted
        // using Environment.getExternalStorageState() before doing this.

        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), APP_IMAGE_FOLDER);
        // This location works best if you want the created images to be shared
        // between applications and persist after your app has been uninstalled.

        // Create the storage directory if it does not exist
        if (! mediaStorageDir.exists()){
            if (! mediaStorageDir.mkdirs()){
                Log.d(APP_IMAGE_FOLDER, "failed to create directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File mediaFile;
        if (type == MEDIA_TYPE_IMAGE){
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "IMG_"+ timeStamp + ".jpg");
        } else if(type == MEDIA_TYPE_VIDEO) {
            mediaFile = new File(mediaStorageDir.getPath() + File.separator +
                    "VID_"+ timeStamp + ".mp4");
        } else {
            return null;
        }

        return mediaFile;
    }

}
