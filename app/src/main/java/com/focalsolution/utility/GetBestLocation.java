package com.focalsolution.utility;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.util.Log;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class GetBestLocation {
	Timer timerTask;
	public long time_out = 15000;
    LocationManager lm;
    ArrayList<Location> locationResult;
    private LocationGetter GOTLOCATION;
    boolean gps_enabled=false;
    boolean network_enabled=false;
    public boolean getLocation(Context c,LocationGetter got)
    {
        //I use LocationResult callback class to pass location value from MyLocation to user code.
    	GOTLOCATION = got;
    	
        if(lm==null)
            lm = (LocationManager) c.getSystemService(Context.LOCATION_SERVICE);
        
        locationResult = new ArrayList<Location>();
           
        Log.i("BR", "Branch start 3");
        //exceptions will be thrown if provider is not permitted.
        try{gps_enabled=lm.isProviderEnabled(LocationManager.GPS_PROVIDER);}catch(Exception ex){}
        try{network_enabled=lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);}catch(Exception ex){}

        //don't start listeners if no provider is enabled
        if(!gps_enabled && !network_enabled)
            return false;
        
        Log.i("BR", "Branch listerner");
        if(gps_enabled)
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
        if(network_enabled)
            lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
          
        timerTask=new Timer();
        timerTask.schedule(new GetLastLocation(), time_out);
        
        final TelephonyManager telephony = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
        if (telephony.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
            final GsmCellLocation location = (GsmCellLocation) telephony.getCellLocation();
            if (location != null) {
            	location.getLac();
            }
        }
        
        Log.i("BR", "Branch listerner end");

        return true;
    }

    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {

        	locationResult.add(location);
        	Log.i("BR", "get GPS");
        	if(locationResult.size()>= 2)
        	{
        		lm.removeUpdates(this);
        		lm.removeUpdates(locationListenerNetwork);
        		timerTask.cancel();
        		GOTLOCATION.gotLocation(getBestLocation());
        	}

        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
        	
        	locationResult.add(location);
        	Log.i("BR", "get NETWORK");
        	
        	if(locationResult.size()>= 2)
        	{
        		lm.removeUpdates(this);
        		lm.removeUpdates(locationListenerGps);
        		timerTask.cancel();
        		GOTLOCATION.gotLocation(getBestLocation());
        	}
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    private Location getBestLocation()
    {
    	Location bestLocation = null;
    	boolean providerEnabled = false;
    	
    	for (String str : lm.getAllProviders()) {
    		
    		Log.i("BR", "get location " + str);
    		
    		try
    		{
    			providerEnabled=lm.isProviderEnabled(str);
    		}
    		catch(Exception ex)
    		{
    			providerEnabled=false;
    		}
    		
    		if(providerEnabled)
    		{
    			
    			if (bestLocation == null)
    			{
    				bestLocation = lm.getLastKnownLocation(str);
    				Log.i("BR", "get first best " + bestLocation);
    				continue;
    			}
    			
	    		Location newlocation = lm.getLastKnownLocation(str);
	    		Log.i("BR", "get best " + bestLocation);
	    		Log.i("BR", "get new " + newlocation);
	    		
	    		if(newlocation!=null)
	    		{
		    		if(isBetterLocation(newlocation,bestLocation))
		    			bestLocation = newlocation;
	    		}
    		}
        } 
    	Log.i("BR", "Return location " + bestLocation);
    	return bestLocation;
    }
    class GetLastLocation extends TimerTask {
        @Override
        public void run() {

        	lm.removeUpdates(locationListenerGps);
        	lm.removeUpdates(locationListenerNetwork);
        	
        	GOTLOCATION.gotLocation(getBestLocation());
        }
    }

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
      * @param location  The new Location that you want to evaluate
      * @param currentBestLocation  The current Location fix, to which you want to compare the new one
      */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        if(location.equals(currentBestLocation))
        	return false;
        
        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
        // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
          return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
