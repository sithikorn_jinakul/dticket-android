package com.focalsolution.utility;

import android.location.Location;

public abstract class LocationGetter {
    
	public abstract void gotLocation(Location locationRes);
    
}
