package com.focalsolution.utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.util.Log;
import android.widget.ImageView;

import com.focalsolution.CustomControl.CircleImageView;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

/**
 * Created by sithikorn on 5/16/14 AD.
 */
public class ImageDownloader {
    private static final String LOG_TAG = "ImageDownloader";

    final CacheDatabase cacheDatabase;
    final String DISK_IMAGE_CACHE_SUBDIR = "images";
    final String DefaultCacheDir;
    final Context currentContext;
    int EXP_DAY = 0, EXP_MONTH = 1, EXP_YEAR = 0;
    int connectionTimeOut = 5000;
    int readTimeOut = 4000;

    //Default bg base color
    static int defaultColor = Color.GRAY;

    private LruCache<String, Bitmap> mMemoryCache;

    static AnimationDrawable animationDrawable = null;

    public ImageDownloader(Context c, String cacheFolder, Bitmap imageOnError) {
        currentContext = c;
        this.cacheDatabase = new CacheDatabase(currentContext);

        DefaultCacheDir = cacheFolder + "/" + DISK_IMAGE_CACHE_SUBDIR + "/";
        Util.dirChecker(DefaultCacheDir);

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        //activityManager = (ActivityManager)c.getSystemService(Context.ACTIVITY_SERVICE);
        //int memClass = activityManager.getMemoryClass();

        //HARD_CACHE_CAPACITY = memClass / 16;

        defautlImageOnError = imageOnError;

        defaultScaleType = ImageView.ScaleType.CENTER_CROP;
        /*
        purgeHandler = new Handler();

        purger = new Runnable() {
            public void run() {
                clearCache();
            }
        };
        */

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = (maxMemory / 8);

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return GraphicsHelper.byteSizeOf(bitmap);
            }
        };
    }
    public ImageDownloader(Context c, String cacheFolder) {
        currentContext = c;
        this.cacheDatabase = new CacheDatabase(currentContext);

        DefaultCacheDir = cacheFolder + "/" + DISK_IMAGE_CACHE_SUBDIR + "/";
        Util.dirChecker(DefaultCacheDir);

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        //activityManager = (ActivityManager)c.getSystemService(Context.ACTIVITY_SERVICE);
        //int memClass = activityManager.getMemoryClass();

        //HARD_CACHE_CAPACITY = memClass / 16;

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return GraphicsHelper.byteSizeOf(bitmap);
            }
        };

        /*
        purgeHandler = new Handler();

        purger = new Runnable() {
            public void run() {
                clearCache();
            }
        };
        */
    }
    public ImageDownloader(Context c, String cacheFolder, Bitmap imageOnError, AnimationDrawable animDraw) {
        currentContext = c;
        this.cacheDatabase = new CacheDatabase(currentContext);

        DefaultCacheDir = cacheFolder + "/" + DISK_IMAGE_CACHE_SUBDIR + "/";
        Util.dirChecker(DefaultCacheDir);

        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        //activityManager = (ActivityManager)c.getSystemService(Context.ACTIVITY_SERVICE);
        //int memClass = activityManager.getMemoryClass();

        //HARD_CACHE_CAPACITY = memClass / 16;

        defautlImageOnError = imageOnError;

        animationDrawable = animDraw;

        defaultScaleType = ImageView.ScaleType.CENTER_CROP;

        /*
        purgeHandler = new Handler();

        purger = new Runnable() {
            public void run() {
                clearCache();
            }
        };
        */

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = (maxMemory / 8);

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return GraphicsHelper.byteSizeOf(bitmap);
            }
        };
    }

    static Bitmap defautlImageOnError;

    public void setConnectionTimeOut(int connectionTimeOut) {
        this.connectionTimeOut = connectionTimeOut;
    }

    public void setReadTimeOut(int readTimeOut) {
        this.readTimeOut = readTimeOut;
    }

    private ImageView.ScaleType defaultScaleType = ImageView.ScaleType.CENTER_CROP;
    private ImageView.ScaleType downloadAnimScaleType = ImageView.ScaleType.CENTER;

    public static void setDefautlImageOnError(Bitmap defautlImageOnError) {
        ImageDownloader.defautlImageOnError = defautlImageOnError;
    }

    public static void setDefaultColor(int defaultColor) {
        ImageDownloader.defaultColor = defaultColor;
    }

    public void setDownloadAnimScaleType(ImageView.ScaleType downloadAnimScaleType) {
        this.downloadAnimScaleType = downloadAnimScaleType;
    }

    public void setScaleType(ImageView.ScaleType _defaultScaleType) {
        defaultScaleType = _defaultScaleType;
    }

    public static AnimationDrawable getAnimationDrawable() {
        return animationDrawable;
    }

    /**
     * Download the specified image from the Internet and binds it to the provided ImageView. The
     * binding is immediate if the image is found in the cache and will be done asynchronously
     * otherwise. A null bitmap will be associated to the ImageView if an error occurs.
     *
     * @paramurl The URL of the image to download.
     * @param imageView The ImageView to bind the downloaded image to.
     */
    public void download(final String url, final ImageView imageView) {
        download(url, ImageViewDisplayType.ImageBitmap, imageView);
    }
    public void download(final String url, ImageViewDisplayType type, final ImageView imageView) {
        //resetPurgeTimer();

        final int w = imageView.getWidth() < 1 ? 256 : imageView.getWidth();
        final int h = imageView.getHeight() < 1 ? 256 : imageView.getHeight();

        final ImageData imageData = new ImageData(url, w, h, type);

        if(imageData.url == null)
        {
            showOnImageView(imageView, defautlImageOnError, imageData);
            return;
        }

        if(imageData.url.trim().equals(""))
        {
            showOnImageView(imageView, defautlImageOnError, imageData);
            return;
        }

        Bitmap bitmap = getBitmapFromCache(imageData);

        if (bitmap == null) {
            forceDownload(imageData, imageView);
        } else {
            if(animationDrawable != null && !(imageView instanceof CircleImageView))
            {
                cancelPotentialDownloadAnim(imageData, imageView);
            } else {
                cancelPotentialDownload(imageData, imageView);
            }
            showOnImageView(imageView, bitmap, imageData);
        }
    }

    private void showOnImageView(ImageView iv, Bitmap bmp, ImageData imageData)
    {
        if(iv != null && bmp != null)
            switch (imageData.type)
            {
                case BackgroundDrawable:
                    iv.setScaleType(defaultScaleType);
                    iv.setBackgroundDrawable(GraphicsHelper.convertBitmapToDrawable(bmp, currentContext));
                    break;
                case ImageBitmap:
                    iv.setScaleType(defaultScaleType);
                    iv.setImageBitmap(bmp);
                    break;
            }
    }

    private void showOnImageView(ImageView iv, Drawable drawable, ImageData imageData)
    {
        if(drawable instanceof DownloadAnimation)
        {
            iv.setScaleType(downloadAnimScaleType);
            iv.setImageDrawable(drawable);
            AnimationDrawable ad = (AnimationDrawable) iv.getDrawable();
            ad.start();
        } else {
            switch (imageData.type) {
                case BackgroundDrawable:
                    iv.setScaleType(defaultScaleType);
                    iv.setBackgroundDrawable(drawable);
                    break;
                case ImageBitmap:
                    iv.setScaleType(defaultScaleType);
                    iv.setImageDrawable(drawable);
                    break;
            }
        }
    }

    public void setExpires(int day, int month, int year)
    {
        EXP_DAY = day;
        EXP_MONTH = month;
        EXP_YEAR = year;
    }

    private void forceDownload(ImageData imageData, ImageView imageView) {
        if (imageData.url == null) {
            showOnImageView(imageView, defautlImageOnError, imageData);
            return;
        }

        if(imageData.url.trim().equals(""))
        {
            showOnImageView(imageView, defautlImageOnError, imageData);
            return;
        }

        if(animationDrawable != null  && !(imageView instanceof CircleImageView)) {

            if (cancelPotentialDownloadAnim(imageData, imageView)) {
                BitmapDownloaderTask task = new BitmapDownloaderTask(imageView, imageData);
                DownloadAnimation downloadAnimation = new DownloadAnimation(task);
                showOnImageView(imageView, downloadAnimation, imageData);
                imageView.setMinimumHeight(156);
                task.execute(imageData.url);
            }

        } else {

            if (cancelPotentialDownload(imageData, imageView)) {
                BitmapDownloaderTask task = new BitmapDownloaderTask(imageView, imageData);
                DownloadedDrawable downloadedDrawable = new DownloadedDrawable(task);
                showOnImageView(imageView, downloadedDrawable, imageData);
                imageView.setMinimumHeight(156);
                task.execute(imageData.url);
            }
        }
    }

    /*
    private void getImageFromDiskCache(ImageData imageData, ImageView imageView)
    {
        if (imageData.url == null) {
            imageView.setImageDrawable(null);

            return;
        }
    }
    */

    /**
     * Returns true if the current download has been canceled or if there was no download in
     * progress on this image view.
     * Returns false if the download in progress deals with the same url. The download is not
     * stopped in that case.
     */
    private static boolean cancelPotentialDownload(ImageData imageData, ImageView imageView) {

        BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);

        if (bitmapDownloaderTask != null) {
            String bitmapUrl = bitmapDownloaderTask.imageData.url;
            if ((bitmapUrl == null) || (!bitmapUrl.equals(imageData.url))) {
                bitmapDownloaderTask.cancel(true);
            } else {
                // The same URL is already being downloaded.
                return false;
            }
        }

        return true;
    }

    private static boolean cancelPotentialDownloadAnim(ImageData imageData, ImageView imageView)
    {
        BitmapDownloaderTask anim = getBitmapDownloaderTaskFromAnimationDrawable(imageView);
        if (anim != null) {
            String bitmapUrl = anim.imageData.url;
            if ((bitmapUrl == null) || (!bitmapUrl.equals(imageData.url))) {
                anim.cancel(true);
            } else {
                // The same URL is already being downloaded.
                return false;
            }
        }

        return true;
    }

    /**
     * @param imageView Any imageView
     * @return Retrieve the currently active download task (if any) associated with this imageView.
     * null if there is no such task.
     */
    private static BitmapDownloaderTask getBitmapDownloaderTask(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadedDrawable) {
                DownloadedDrawable downloadedDrawable = (DownloadedDrawable)drawable;
                return downloadedDrawable.getBitmapDownloaderTask();
            }

            drawable = imageView.getBackground();
            if (drawable instanceof DownloadedDrawable) {
                DownloadedDrawable downloadedDrawable = (DownloadedDrawable)drawable;
                return downloadedDrawable.getBitmapDownloaderTask();
            }
        }
        return null;
    }

    private static BitmapDownloaderTask getBitmapDownloaderTaskFromAnimationDrawable(ImageView imageView) {
        if (imageView != null) {
            Drawable drawable = imageView.getDrawable();
            if (drawable instanceof DownloadAnimation) {
                DownloadAnimation downloadAnimation = (DownloadAnimation)drawable;
                return downloadAnimation.getBitmapDownloaderTask();
            }
        }
        return null;
    }

    Bitmap downloadToCache(ImageData imageData){

        try {

            String[] str = imageData.url.split("/");
            String fileType = str[str.length-1];
            str = fileType.split("\\.");
            fileType = str[str.length-1];
            str = fileType.split("\\?");
            fileType = str[0];
            String fileName = String.format("%d." + fileType, System.currentTimeMillis());
            URL url = new URL(imageData.url);
            URLConnection connection = url.openConnection();
            connection.setConnectTimeout(connectionTimeOut);
            connection.setReadTimeout(readTimeOut);
            connection.connect();
            // download the file
            InputStream input = new BufferedInputStream(url.openStream());
            OutputStream output = new FileOutputStream(DefaultCacheDir + fileName);

            byte data[] = new byte[2048];
            //long total = 0;
            int count;
            while ((count = input.read(data)) != -1) {
                //total += count;
                // publishing the progress....
                //Bundle resultData = new Bundle();
                //resultData.putInt("progress" ,(int) (0));
                //receiver.send(UPDATE_PROGRESS, resultData);
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();

            addImageToDiskCache(imageData.url, DefaultCacheDir + fileName);

            if(imageData.width > 0 && imageData.height > 0)
                return getImageFromDiskCache(imageData.url, imageData.width, imageData.height);

            return null;

        }
        catch (SocketTimeoutException e) {
            e.printStackTrace();
            if(defautlImageOnError != null) return defautlImageOnError;
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
            if(defautlImageOnError != null) return defautlImageOnError;
        } catch (IOException e) {
            e.printStackTrace();
            if(defautlImageOnError != null) return defautlImageOnError;
        } catch (Exception e) {
            e.printStackTrace();
            if(defautlImageOnError != null) return defautlImageOnError;
        }
        return null;
    }

    /**
     * The actual AsyncTask that will asynchronously download the image.
     */
    class BitmapDownloaderTask extends AsyncTask<String, Void, Bitmap> {
        private ImageData imageData;
        private final WeakReference<ImageView> imageViewReference;

        public BitmapDownloaderTask(ImageView imageView, ImageData _imageData) {
            imageViewReference = new WeakReference<ImageView>(imageView);
            imageData = _imageData;
        }

        /**
         * Actual download method.
         */
        @Override
        protected Bitmap doInBackground(String... params) {
            imageData.url = params[0];
            if(imageData.url.toLowerCase().startsWith("http://") || imageData.url.toLowerCase().startsWith("https://")) {
                if (hasImageFromDiskCache(imageData.url)) {
                    /*try {
                        Thread.currentThread().sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }*/
                    return getImageFromDiskCache(imageData.url, imageData.width, imageData.height);
                } else {
                    return downloadToCache(imageData);
                }
            } else {
                return getImageFromLocal(imageData.url, imageData.width, imageData.height);
            }
        }

        /**
         * Once the image is downloaded, associates it to the imageView
         */
        @Override
        protected void onPostExecute(Bitmap bitmap) {
            if (isCancelled()) {
                bitmap = null;
            }

            if(bitmap != null) {
                if(GraphicsHelper.byteSizeOf(bitmap) <= 1000000 && !bitmap.equals(defautlImageOnError))
                    addBitmapToCache(imageData, bitmap);
            }

            if (imageViewReference != null) {
                ImageView imageView = imageViewReference.get();

                BitmapDownloaderTask bitmapDownloaderTask = getBitmapDownloaderTask(imageView);
                // Change bitmap only if this process is still associated with it
                // Or if we don't use any bitmap to task association (NO_DOWNLOADED_DRAWABLE mode)

                if (this == bitmapDownloaderTask) {
                    showOnImageView(imageView, bitmap, imageData);
                    //imageView.setImageBitmap(bitmap);
                }

                if(animationDrawable != null)
                {
                    bitmapDownloaderTask = getBitmapDownloaderTaskFromAnimationDrawable(imageView);
                    // Change bitmap only if this process is still associated with it
                    // Or if we don't use any bitmap to task association (NO_DOWNLOADED_DRAWABLE mode)

                    if (this == bitmapDownloaderTask) {
                        showOnImageView(imageView, bitmap, imageData);
                        //imageView.setImageBitmap(bitmap);
                    }
                }
            }
        }
    }

    /**
     * A fake Drawable that will be attached to the imageView while the download is in progress.
     *
     * <p>Contains a reference to the actual download task, so that a download task can be stopped
     * if a new binding is required, and makes sure that only the last started download process can
     * bind its result, independently of the download finish order.</p>
     */
    static class DownloadedDrawable extends ColorDrawable {
        private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

        public DownloadedDrawable(BitmapDownloaderTask bitmapDownloaderTask) {
            super(defaultColor);
            bitmapDownloaderTaskReference =
                    new WeakReference<BitmapDownloaderTask>(bitmapDownloaderTask);
        }

        public BitmapDownloaderTask getBitmapDownloaderTask() {
            return bitmapDownloaderTaskReference.get();
        }
    }

    static class DownloadAnimation extends AnimationDrawable
    {
        private final WeakReference<BitmapDownloaderTask> bitmapDownloaderTaskReference;

        public DownloadAnimation(BitmapDownloaderTask bitmapDownloaderTask) {
            super();
            bitmapDownloaderTaskReference =
                    new WeakReference<BitmapDownloaderTask>(bitmapDownloaderTask);

            for (int i = 0; i < animationDrawable.getNumberOfFrames(); i++) {
                addFrame(animationDrawable.getFrame(i), animationDrawable.getDuration(i));
            }

            setOneShot(false);
        }

        public BitmapDownloaderTask getBitmapDownloaderTask() {
            return bitmapDownloaderTaskReference.get();
        }
    }

    private void addBitmapToCache(ImageData imageData, Bitmap bmp)
    {
        synchronized (mMemoryCache) {
            if (getBitmapFromCache(imageData) == null) {
                mMemoryCache.put(getCacheKey(imageData.url, imageData.width, imageData.height), bmp);
            }
        }
    }

    private String getCacheKey(String url, int width, int height)
    {
        return url + "_(w)" + width + "_(h)" + height;
    }

    /**
     * @paramurl The URL of the image that will be retrieved from the cache.
     * @return The cached bitmap or null if it was not found.
     */

    public void downloadAndPushToCache(final List<String> keys)
    {
        setExpires(0,0,25);

        new Thread(new Runnable() {
            @Override
            public void run() {

                for (String key : keys) {

                    String path = getImagePathFromDiskCache(key);
                    if (path != "") {
                        File imageFile = new File(path);
                        if (imageFile.exists()) {
                            return;
                        } else {
                            deleteImageFromDatabase(key);
                        }
                    }

                    ImageData d = new ImageData(key, 0, 0, ImageViewDisplayType.ImageBitmap);
                    downloadToCache(d);
                }
            }
        }).start();
    }

    public String downloadDiskCache(String key)
    {
        setExpires(0,0,25);

        String path = getImagePathFromDiskCache(key);
        if (path != "") {
            File imageFile = new File(path);
            if (imageFile.exists()) {
                return null;
            } else {
                deleteImageFromDatabase(key);
            }
        }

        ImageData d = new ImageData(key, 0, 0, ImageViewDisplayType.ImageBitmap);
        downloadToCache(d);

        return getImagePathFromDiskCache(key);
    }

    private Bitmap getBitmapFromCache(final ImageData imageData) {
        synchronized (mMemoryCache) {
            return mMemoryCache.get(getCacheKey(imageData.url, imageData.width, imageData.height));
        }
    }

    public synchronized Bitmap getImageFromLocal(String key, int width, int height)
    {
        int drawable_id = -1;
        try {
            drawable_id = Integer.parseInt(key);
        }
        catch (Exception ex)
        {
            drawable_id = -1;
        }
        if(drawable_id == -1) {
            try {
                File imageFile = new File(key);
                BitmapFactory.Options o = new BitmapFactory.Options();
                o.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(new FileInputStream(imageFile), null, o);

                o.inSampleSize = GraphicsHelper.calculateInSampleSize(o, width, height);
                o.inJustDecodeBounds = false;

                Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(imageFile), null, o);

                return bmp;
            } catch (Exception ex) {
                ex.printStackTrace();
                if(defautlImageOnError != null) return defautlImageOnError;
            }
        } else {
            try {
                Drawable drawable = currentContext.getResources().getDrawable(drawable_id);
                return GraphicsHelper.convertDrawableToBitmap(drawable, currentContext);
            } catch (Exception ex) {
                ex.printStackTrace();
                if(defautlImageOnError != null) return defautlImageOnError;
            }
        }

        return null;
    }

    //region cache database
    public synchronized boolean hasImageFromDiskCache(String key)
    {
        SQLiteDatabase db = cacheDatabase.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + cacheDatabase.Table_CacheFile + " where key_url = '" + key + "'", null);
        if (c != null ) {
            if  (c.moveToFirst()) {
                String pathName = c.getString(c.getColumnIndex("save_path"));
                File imageFile = new File(pathName);
                if(imageFile.exists())
                {
                    c.close();
                    db.close();
                    return true;
                }

                deleteImageFromDatabase(key);
            }
        }
        c.close();
        db.close();
        return false;
    }
    public synchronized boolean deleteImageFromDatabase(String key)
    {
        String pathName = getImagePathFromDiskCache(key);
        if(pathName!=null) {
            File imageFile = new File(pathName);
            if (imageFile.exists()) {
                imageFile.delete();
            }
        }

        SQLiteDatabase dbd = cacheDatabase.getWritableDatabase();
        boolean res = dbd.delete(CacheDatabase.Table_CacheFile, "key_url = '" + key + "'", null) > 0;
        dbd.close();
        return res;
    }
    private synchronized boolean clearOldCacheAndFile(long today)
    {
        SQLiteDatabase db = cacheDatabase.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + cacheDatabase.Table_CacheFile + " where expires < " + today, null);
        if (c != null ) {
            if  (c.moveToFirst()) {
                String pathName = c.getString(c.getColumnIndex("save_path"));
                String key = c.getString(c.getColumnIndex("key_url"));
                File imageFile = new File(pathName);
                if(imageFile.exists())
                {
                    imageFile.delete();
                }
                deleteImageFromDatabase(key);
            }
        }
        c.close();
        db.close();
        return true;
    }
    public synchronized String getImagePathFromDiskCache(String key)
    {
        SQLiteDatabase db = cacheDatabase.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + cacheDatabase.Table_CacheFile + " where key_url = '" + key + "'", null);
        if (c != null ) {
            if (c.moveToFirst()) {
                String pathName = c.getString(c.getColumnIndex("save_path"));
                return pathName;
            }
        }
        return "";
    }
    private synchronized Bitmap getImageFromDiskCache(String key, int width, int height)
    {
        SQLiteDatabase db = cacheDatabase.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + cacheDatabase.Table_CacheFile + " where key_url = '" + key + "'", null);
        if (c != null ) {
            if  (c.moveToFirst()) {
                String pathName = c.getString(c.getColumnIndex("save_path"));
                long expires = c.getLong(c.getColumnIndex("expires"));
                long today = Util.getCurrentTimeStamp();
                if(expires < today)
                {
                    deleteImageFromDatabase(key);
                    Util.DeleteDirRecursive(new File(pathName));
                    c.close();
                    db.close();
                    return null;
                }

                try {

                    File imageFile = new File(pathName);

                    BitmapFactory.Options o = new BitmapFactory.Options();
                    o.inJustDecodeBounds = true;
                    BitmapFactory.decodeStream(new FileInputStream(imageFile), null, o);

                    o.inSampleSize = GraphicsHelper.calculateInSampleSize(o, width, height);
                    o.inJustDecodeBounds = false;
                    Log.d(LOG_TAG, "width: " + width + ", height: " + height);

                    Bitmap bmp = BitmapFactory.decodeStream(new FileInputStream(imageFile), null, o);
                    Log.d(LOG_TAG, "Bitmap Memory :" + GraphicsHelper.byteSizeOf(bmp));
                    Log.d(LOG_TAG, "o.w: " + o.outWidth + ", o.h: " + o.outHeight);
                    return bmp;
                }
                catch (OutOfMemoryError ex)
                {
                    Log.e(LOG_TAG, "****\nOut of memory\n****");
                    ex.printStackTrace();
                    mMemoryCache.evictAll();
                    System.gc();
                    //if(defautlImageOnError != null) return defautlImageOnError;
                }
                catch (Exception ex)
                {
                    ex.printStackTrace();
                    if(defautlImageOnError != null) return defautlImageOnError;
                } finally {
                    c.close();
                    db.close();
                }
            }
        }

        return null;
    }
    public synchronized void addImageToDiskCache(String key, String pathAndFileName)
    {
        if(hasImageFromDiskCache(key))
            deleteImageFromDatabase(key);

        SQLiteDatabase db = cacheDatabase.getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("key_url", key);
        cv.put("save_path", pathAndFileName);
        cv.put("expires", Util.addCurrentTimeStamp(EXP_DAY, EXP_MONTH, EXP_YEAR));
        db.insert(CacheDatabase.Table_CacheFile, null, cv);
        db.close();
    }

    public class CacheDatabase extends SQLiteOpenHelper {

        // Database Version
        private static final int DATABASE_VERSION = 1;

        // Database Name
        private static final String DATABASE_NAME = "caching.db";

        // Table Name
        public static final String Table_CacheFile = "cache_file";

        public CacheDatabase(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub

            db.execSQL("CREATE TABLE " + Table_CacheFile +
                    "(key_url VARCHAR ," +
                    " save_path VARCHAR," +
                    " expires int," +
                    " PRIMARY KEY (key_url));");

            Log.d("CREATE TABLE", "Create Table Successfully.");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub

            if(newVersion > 1)
            {
                db.execSQL("DROP TABLE  IF EXISTS " + Table_CacheFile);
                onCreate(db);
            }
        }

    }
    //endregion

    public class ImageData
    {
        public String url;
        public int width;
        public int height;
        public ImageViewDisplayType type;

        public ImageData(String url, int width, int height, ImageViewDisplayType type) {
            this.url = url;
            this.width = width;
            this.height = height;
            this.type = type;
        }
    }

    public enum ImageViewDisplayType {
        ImageBitmap,
        BackgroundDrawable
    }

}
