package com.focalsolution.webservice.Core;

import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.params.CookiePolicy;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.ByteArrayBuffer;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ServiceCall {
    DefaultHttpClient httpClient;
    HttpContext localContext;
    private String ret;
    final String LOGTAG = "ServiceCall";

    HttpResponse response = null;
    HttpPost httpPost = null;
    HttpGet httpGet = null;
    String webServiceUrl;

    //The serviceName should be the name of the Service you are going to be using.
    public ServiceCall(String serviceName){
        HttpParams myParams = new BasicHttpParams();

        HttpConnectionParams.setConnectionTimeout(myParams, 30000);
        HttpConnectionParams.setSoTimeout(myParams, 30000);
        //httpClient = new DefaultHttpClient(myParams);
        localContext = new BasicHttpContext();
        webServiceUrl = serviceName;

    }

    //Use this method to do a HttpPost\WebInvoke on a Web Service
    public String webInvoke(String methodName, Map<String, Object> params) {

        JSONObject jsonObject = new JSONObject();

        for (Map.Entry<String, Object> param : params.entrySet()){
            try {
                jsonObject.put(param.getKey(), param.getValue());
            }
            catch (JSONException e) {
                Log.e(LOGTAG, "JSONException : " + e);
            }
        }
        return webInvoke(methodName,  jsonObject.toString(), "application/json");
    }
    public String webInvokeByQuery(String methodName, List<ServiceParameter> params)
    {
        if(params==null)
            return webInvokeByQuery(methodName,  null, null);

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(params.size());

        for (ServiceParameter p : params) {
	    		/*
	    		try {
					
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}  
				*/
            try {
                String v = p.Value.toString();//URLEncoder.encode(p.Value.toString(),"UTF-8");
                nameValuePairs.add(new BasicNameValuePair(p.Key, v));
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            //data += p.Key + "=" + URLEncoder.encode(p.Value.toString(),"UTF-8") + "&";

        }

        return webInvokeByQuery(methodName,  nameValuePairs, null);
    }
    private String webInvokeByQuery(String methodName, List<NameValuePair> data, String contentType) {
        ret = null;

        //httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2109);

        //httppost = new HttpPost(serverURL);

        //httpPost.setHeader("User-Agent", "SET YOUR USER AGENT STRING HERE");
        //httpPost.setHeader("Accept", "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");
	        /*
	        if (contentType != null) {
	            httpPost.setHeader("Content-Type", contentType);
	        } else {
	            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
	        }
	        */
        try {
            httpClient = new DefaultHttpClient();
            HttpPost hPost = new HttpPost(webServiceUrl + methodName);
            if(data!=null)
            {
                hPost.setEntity(new UrlEncodedFormEntity(data, HTTP.UTF_8));
            }
            response = httpClient.execute(hPost);

            InputStream is = response.getEntity().getContent();
            BufferedInputStream bis = new BufferedInputStream(is);
            ByteArrayBuffer baf = new ByteArrayBuffer(20);

            int current = 0;
            while((current = bis.read()) != -1)
            {
                baf.append((byte)current);
            }

            ret = new String(baf.toByteArray());

        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        Log.e(LOGTAG, webServiceUrl + methodName + "?" + data);
        Log.e(LOGTAG, ret);

        return ret;
    }

    private String webInvoke(String methodName, String data, String contentType) {
        ret = null;

        httpClient.getParams().setParameter(ClientPNames.COOKIE_POLICY, CookiePolicy.RFC_2109);
        httpPost = new HttpPost(webServiceUrl + methodName);
        response = null;

        StringEntity tmp = null;

        //httpPost.setHeader("User-Agent", "SET YOUR USER AGENT STRING HERE");
        httpPost.setHeader("Accept", "text/html,application/xml,application/xhtml+xml,text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5");

        if (contentType != null) {
            httpPost.setHeader("Content-Type", contentType);
        } else {
            httpPost.setHeader("Content-Type", "application/x-www-form-urlencoded");
        }

        try {
            tmp = new StringEntity(data,"UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.e(LOGTAG, "HttpUtils : UnsupportedEncodingException : " + e);
        }

        httpPost.setEntity(tmp);

        Log.e(LOGTAG, webServiceUrl + methodName + "?" + data);

        try {
            response = httpClient.execute(httpPost,localContext);

            if (response != null) {
                ret = EntityUtils.toString(response.getEntity());
            }
        } catch (Exception e) {
            Log.e(LOGTAG, "HttpUtils: " + e);
        }
        Log.e(LOGTAG, ret);
        return ret;
    }

    //Use this method to do a HttpGet/WebGet on the web service
    public String webGet(String methodName, Map<String, String> params) {
        String getUrl = webServiceUrl + methodName;

        int i = 0;
        for (Map.Entry<String, String> param : params.entrySet())
        {
            if(i == 0){
                getUrl += "?";
            }
            else{
                getUrl += "&";
            }

            try {
                getUrl += param.getKey() + "=" + URLEncoder.encode(param.getValue(), "UTF-8");
            } catch (UnsupportedEncodingException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            i++;
        }

        httpGet = new HttpGet(getUrl);
        Log.e(LOGTAG, "Web getUrl : " + getUrl);

        try {
            response = httpClient.execute(httpGet);
        } catch (Exception e) {
            Log.e(LOGTAG, e.getMessage());
        }

        // we assume that the response body contains the error message
        try {
            ret = EntityUtils.toString(response.getEntity());
        } catch (IOException e) {
            Log.e(LOGTAG, e.getMessage());
        }
        Log.e(LOGTAG, ret);
        return ret;
    }

    public static JSONObject Object(Object o){
        try {
            return new JSONObject(new Gson().toJson(o));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public InputStream getHttpStream(String urlString) throws IOException {
        InputStream in = null;
        int response = -1;

        URL url = new URL(urlString);
        URLConnection conn = url.openConnection();

        if (!(conn instanceof HttpURLConnection))
            throw new IOException("Not an HTTP connection");

        try{
            HttpURLConnection httpConn = (HttpURLConnection) conn;
            httpConn.setAllowUserInteraction(false);
            httpConn.setInstanceFollowRedirects(true);
            httpConn.setRequestMethod("GET");
            httpConn.connect();

            response = httpConn.getResponseCode();

            if (response == HttpURLConnection.HTTP_OK) {
                in = httpConn.getInputStream();
            }
        } catch (Exception e) {
            throw new IOException("Error connecting");
        } // end try-catch

        return in;
    }

    public void clearCookies() {
        httpClient.getCookieStore().clear();
    }

    public void abort() {
        try {
            if (httpClient != null) {
                System.out.println("Abort.");
                httpPost.abort();
            }
        } catch (Exception e) {
            System.out.println("DASTA" + e);
        }
    }

    public String uploadFile(List<FileUploadData> fileDatas, List<ServiceParameter> params) throws Exception
    {
        return uploadFile("", fileDatas, params);
    }

    public String uploadFile(String serviceMethod, List<FileUploadData> fileDatas, List<ServiceParameter> params) throws Exception {
        //String urlString = "http://10.0.2.2/upload_test/upload_media_test.php";
        try
        {
            HttpClient client = new DefaultHttpClient();
            HttpPost post = new HttpPost(webServiceUrl + serviceMethod);
            MultipartEntity reqEntity = new MultipartEntity();

            for(FileUploadData f : fileDatas) {
                File file = new File(f.filePaths);
                if(!file.exists())
                    throw new FileNotFoundException();

                FileBody fileBody = new FileBody(file);
                reqEntity.addPart(f.ParametorName, fileBody);
            }

            for(ServiceParameter p : params)
            {
                reqEntity.addPart(p.Key, new StringBody(p.Value.toString()));
            }

            post.setEntity(reqEntity);
            HttpResponse response = client.execute(post, localContext);
            Log.e(LOGTAG, webServiceUrl + serviceMethod + "?" + params.toString());
            HttpEntity resEntity = response.getEntity();
            final String response_str = EntityUtils.toString(resEntity);
            Log.e(LOGTAG, "response=" + response_str);
            if (resEntity != null) {
                return response_str;
            }

            throw new Exception(response_str);
        }
        catch (Exception ex){
            Log.e("Debug", "error: " + ex.getMessage(), ex);
            throw new Exception(ex.getMessage());
        }
    }
}

