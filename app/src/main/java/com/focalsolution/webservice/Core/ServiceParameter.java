package com.focalsolution.webservice.Core;

public class ServiceParameter {
	public String Key;
	public Object Value;
	public ServiceParameter(String k, Object v)
	{
		this.Key = k;
		this.Value = v;
	}
	public ServiceParameter()
	{
	
	}

    @Override
    public String toString() {
        return this.Key + ":" + this.Value + " ";
    }
}