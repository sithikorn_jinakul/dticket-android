package com.focalsolution.webservice;

import android.content.Context;
import android.util.Log;

import com.focalsolution.utility.Util;
import com.focalsolution.webservice.Core.ServiceCall;
import com.focalsolution.webservice.Core.ServiceParameter;
import com.focalsolution.webservice.Entity.EventDetail;
import com.focalsolution.webservice.Entity.EventDetailResponse;
import com.focalsolution.webservice.Entity.EventList;
import com.focalsolution.webservice.Entity.EventListResponse;
import com.focalsolution.webservice.Entity.Ticket;
import com.focalsolution.webservice.Entity.TicketDataResponse;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

public class ServiceManager {

    private String device_id,device_type="Android",device_name,model_name,os_version;
    long timeStamp = 0;
    final String LOGTAG = "ServiceManager";

    private String ServiceURL ="http://dev-dticket.focalsolution.com/api/v1/";

    List<ServiceParameter> params;

    public ServiceManager(Context c, String deviceID, String deviceName, String modelName, String osVersion)
    {
        this.device_id = deviceID;
        this.device_name = deviceName;
        this.model_name = modelName;
        this.os_version = osVersion;

        params = new LinkedList<ServiceParameter>();
        //initParameter();
        timeStamp = Util.getCurrentTimeStamp();
    }

    private void initParameter()
    {
        params.add(new ServiceParameter("device_id", this.device_id));
        params.add(new ServiceParameter("device_type", this.device_type));
        params.add(new ServiceParameter("device_name", this.device_name));
        params.add(new ServiceParameter("model_name", this.model_name));
        params.add(new ServiceParameter("os_version", this.os_version));
    }

    public List<EventList> getEvent()
    {
        initParameter();
        Log.w(LOGTAG, LOGTAG + "." + new Exception().getStackTrace()[0].getMethodName() + "()");

        ServiceCall webService = new ServiceCall(ServiceURL);

        //params.add(new ServiceParameter("item_id", item_id));

        String response = webService.webInvokeByQuery("events", params);
        try
        {
            Gson gson = new Gson(); // GSON object
            Type collectionType = new TypeToken<EventListResponse>(){}.getType();
            EventListResponse res = gson.fromJson(response, collectionType);
            return res.data.events;
        }
        catch(Exception e)
        {
            Log.w("Error: ", e.getMessage());
        }

        return null;
    }

    public EventDetail getEvent(int eventID)
    {
        initParameter();
        Log.w(LOGTAG, LOGTAG + "." + new Exception().getStackTrace()[0].getMethodName() + "()");

        ServiceCall webService = new ServiceCall(ServiceURL);

        //params.add(new ServiceParameter("id", eventID));

        String response = webService.webInvokeByQuery("events/"+ eventID, params);

        try
        {
            Gson gson = new Gson(); // GSON object
            Type collectionType = new TypeToken<EventDetailResponse>(){}.getType();
            EventDetailResponse res = gson.fromJson(response, collectionType);
            return res.data;
        }
        catch(Exception e)
        {
            Log.w("Error: ", e.getMessage());
        }

        return null;
    }

    public boolean bookingTicket(int eventID, int ticketID, int numberOfTicket, int userID)
    {
        initParameter();
        Log.w(LOGTAG, LOGTAG + "." + new Exception().getStackTrace()[0].getMethodName() + "()");

        ServiceCall webService = new ServiceCall(ServiceURL);

        params.add(new ServiceParameter("event_id", eventID));
        params.add(new ServiceParameter("ticket_type_id", ticketID));
        params.add(new ServiceParameter("number_of_tickets", numberOfTicket));
        params.add(new ServiceParameter("user_id", userID));

        String response = webService.webInvokeByQuery("user/ticket/booking", params);

        try
        {
            //Gson gson = new Gson(); // GSON object
            //Type collectionType = new TypeToken<EventDetailResponse>(){}.getType();
            //EventDetailResponse res = gson.fromJson(response, collectionType);
            return response.indexOf("\"success\":true") >= 0 ? true : false;
        }
        catch(Exception e)
        {
            Log.w("Error: ", e.getMessage());
        }

        return false;
    }

    public boolean setTicketUsed(int ticket_id, int userID)
    {
        initParameter();
        Log.w(LOGTAG, LOGTAG + "." + new Exception().getStackTrace()[0].getMethodName() + "()");

        ServiceCall webService = new ServiceCall(ServiceURL);

        params.add(new ServiceParameter("ticket_id", ticket_id));
        params.add(new ServiceParameter("user_id", userID));

        String response = webService.webInvokeByQuery("user/ticket/use", params);

        try
        {
            //Gson gson = new Gson(); // GSON object
            //Type collectionType = new TypeToken<EventDetailResponse>(){}.getType();
            //EventDetailResponse res = gson.fromJson(response, collectionType);
            return response.indexOf("\"success\":true") >= 0 ? true : false;
        }
        catch(Exception e)
        {
            Log.w("Error: ", e.getMessage());
        }

        return false;
    }

    public List<Ticket> getTicket(int userID)
    {
        initParameter();
        Log.w(LOGTAG, LOGTAG + "." + new Exception().getStackTrace()[0].getMethodName() + "()");

        ServiceCall webService = new ServiceCall(ServiceURL);

        params.add(new ServiceParameter("user_id", userID));

        String response = webService.webInvokeByQuery("user/ticket", params);

        try
        {
            Gson gson = new Gson(); // GSON object
            Type collectionType = new TypeToken<TicketDataResponse>(){}.getType();
            TicketDataResponse res = gson.fromJson(response, collectionType);
            return res.data.tickets;
        }
        catch(Exception e)
        {
            Log.w("Error: ", e.getMessage());
        }

        return null;
    }

    public class TokenFailException extends Exception
    {
        public TokenFailException() { }
        public TokenFailException(String detailMessage) {
            super(detailMessage);
        }
    }

    public class ServiceFailException extends Exception
    {
        public ServiceFailException() { }
        public ServiceFailException(String detailMessage) {
            super(detailMessage);
        }
    }
}
