package com.focalsolution.webservice.Entity;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sithikorn on 9/25/14 AD.
 */
public class DataResponse {
    public List<EventList> events;

    public DataResponse() {
        events = new LinkedList<EventList>();
    }
}
