package com.focalsolution.webservice.Entity;

import com.focalsolution.utility.Util;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sithikorn on 9/25/14 AD.
 */
public class EventDetail {

    public int id; //": "39",
    public String name; //": "กิจกรรมแมวน้ำ",
    public String detail; //": "<p>แมวน้ำอีเว้นท์</p>",
    public String start_time; //": "2014-09-25 20:00:00",
    public String end_time; //": "2014-09-29 14:00:00",
    public String location_name; //": "ทะเล",

    public double location_lat; //": "13.753741",
    public double location_lng; //": "100.572868",
    public String location_formatted_address; //": "Thale Bangkok, Bang Kapi, Huai Khwang, Bangkok 10310, Thailand",

    public Photo cover_photo;

    public List<Photo> photos;

    public List<TicketType> ticket_types;

    public EventDetail() {
        photos = new LinkedList<Photo>();
        ticket_types = new LinkedList<TicketType>();
    }

    public String getStartTime(String formath)
    {
        return Util.getDateString(convertDateTimeStringToTimestamp(this.start_time), formath);
    }

    public String getEndTime(String formath)
    {
        return Util.getDateString(convertDateTimeStringToTimestamp(this.end_time), formath);
    }

    private long convertDateStringToTimestamp(String date)
    {
        String[] str = date.split("-");
        int day = Util.toInterger(str[2], -1);
        int month = Util.toInterger(str[1], -1);
        int year = Util.toInterger(str[0], -1);
        return Util.getCurrentTimeStamp(day, month, year);
    }

    private long convertDateTimeStringToTimestamp(String dateTime)
    {
        String[] str = dateTime.split(" ");
        String[] date = str[0].split("-");
        int day = Util.toInterger(date[2], -1);
        int month = Util.toInterger(date[1], -1);
        int year = Util.toInterger(date[0], -1);

        String[] time = str[1].split(":");
        int h = Util.toInterger(date[0], -1);
        int m = Util.toInterger(date[1], -1);
        int s = Util.toInterger(date[2], -1);
        return Util.getCurrentTimeStamp(year, month, day, h, m, s);
    }
}
