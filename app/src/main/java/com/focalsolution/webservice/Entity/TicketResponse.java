package com.focalsolution.webservice.Entity;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sithikorn on 9/26/14 AD.
 */
public class TicketResponse {
    public List<Ticket> tickets;

    public TicketResponse() {
        tickets = new LinkedList<Ticket>();
    }
}
