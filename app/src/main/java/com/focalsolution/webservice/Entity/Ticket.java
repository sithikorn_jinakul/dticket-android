package com.focalsolution.webservice.Entity;

/**
 * Created by sithikorn on 9/26/14 AD.
 */
public class Ticket {
    public int id; //": "0",
    public String code; //": "64bb4886-d4aa-4bbd-9d2c-b03ccce56b90",
    public boolean used; //": false,
    public String used_at; //": null,
    public int user_id; //": "9",
    public int the_event_id; //": "39",
    public int ticket_type_id; //": "2",

    public TicketType ticket_type;

    public EventDetail the_event;
}
