package com.focalsolution.webservice.Entity;

import com.focalsolution.utility.Util;

/**
 * Created by sithikorn on 9/25/14 AD.
 */
public class EventList {

    public int id; //: "39",
    public String name; //": "กิจกรรมแมวน้ำ",
    public String start_time; //": "2014-09-25 20:00:00",
    public String end_time; //": "2014-09-29 14:00:00",
    public String location_name; //": "ทะเล",
    public double price = 0.0f;
    public Photo cover_photo;

    public String getStartTime(String formath)
    {
        return Util.getDateString(convertDateTimeStringToTimestamp(this.start_time), formath);
    }

    public String getEndTime(String formath)
    {
        return Util.getDateString(convertDateTimeStringToTimestamp(this.end_time), formath);
    }

    private long convertDateStringToTimestamp(String date)
    {
        String[] str = date.split("-");
        int day = Util.toInterger(str[2], -1);
        int month = Util.toInterger(str[1], -1);
        int year = Util.toInterger(str[0], -1);
        return Util.getCurrentTimeStamp(day, month, year);
    }

    private long convertDateTimeStringToTimestamp(String dateTime)
    {
        String[] str = dateTime.split(" ");
        String[] date = str[0].split("-");
        int day = Util.toInterger(date[2], -1);
        int month = Util.toInterger(date[1], -1);
        int year = Util.toInterger(date[0], -1);

        String[] time = str[1].split(":");
        int h = Util.toInterger(date[0], -1);
        int m = Util.toInterger(date[1], -1);
        int s = Util.toInterger(date[2], -1);
        return Util.getCurrentTimeStamp(year, month, day, h, m, s);
    }

    /*
            "cover_photo": {
        "id": "5",
                "the_event_id": "39",
                "filename": "dd0a666c27bdff6191dd8e7391136735.jpg",
                "url": "http://dev-ticketproject.focalsolution.com/upload/events/39/photos/dd0a666c27bdff6191dd8e7391136735.jpg"
    }
    */
}
