package com.focalsolution.webservice.ServiceStorage;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by sithikorn on 8/21/14 AD.
 */
public class ServiceStorageManager {

    ServiceStorageDB serviceDB;
    protected static final String TABLE_NAME = "service_table";

    public ServiceStorageManager(Context c)
    {
        serviceDB = new ServiceStorageDB(c);
        Log.i("database", "0.db create");
    }

    public ServiceStoreEntity insert(ServiceStoreEntity s)
    {
        Log.i("database", "insertParkName");
        ServiceStoreEntity pt = select(s.serviceUrl, s.serviceParametor);
        if(pt!=null)
        {
            //return pt;
            delete(s.serviceUrl, s.serviceParametor);
        }

        ContentValues cv = new ContentValues();
        cv.put("url", s.serviceUrl);
        cv.put("para", s.serviceParametor);
        cv.put("result", s.serviceResult);

        SQLiteDatabase db = serviceDB.getWritableDatabase();
        db.insert(TABLE_NAME, null, cv);
        db.close();
        return select(s.serviceUrl, s.serviceParametor);
    }

    public ServiceStoreEntity select(String url, String para)
    {
        Log.i("database", "getParkNameByID");
        SQLiteDatabase db = serviceDB.getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM " + TABLE_NAME + " where url='" + url + "' and para = '" + para + "' ", null);
        ServiceStoreEntity se = null;
        if(c!=null)
            if (c.moveToFirst()) {
                se = TrasulateToServiceStoreEntity(c);
            }
        c.close();
        db.close();
        return  se;
    }

    public void delete(String url, String para)
    {
        String[] key = {url, para};
        SQLiteDatabase db = serviceDB.getWritableDatabase();
        db.delete(TABLE_NAME ,
                " url = ? and para = ? ",
                key);
        db.close();
    }

    private ServiceStoreEntity TrasulateToServiceStoreEntity(Cursor c)
    {
        ServiceStoreEntity se = new ServiceStoreEntity(
                c.getString(c.getColumnIndex("url")),
                c.getString(c.getColumnIndex("para")),
                c.getString(c.getColumnIndex("result")));
        return se;
    }

    private class ServiceStorageDB extends SQLiteOpenHelper {

        // Database Version
        private static final int DATABASE_VERSION = 1;

        // Database Name
        private static final String DATABASE_NAME = "service_storage.db";

        protected ServiceStorageDB(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            // TODO Auto-generated method stub

            db.execSQL("CREATE TABLE '" + TABLE_NAME + "' "
                    + " ( 'url' VARCHAR NOT NULL , "
                    + " 'para' VARCHAR NOT NULL , "
                    + " 'result' VARCHAR NOT NULL , "
                    + " PRIMARY KEY ('url', 'para'));");

            Log.d("CREATE TABLE", "Create Table Successfully.");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub

        }
    }
}
