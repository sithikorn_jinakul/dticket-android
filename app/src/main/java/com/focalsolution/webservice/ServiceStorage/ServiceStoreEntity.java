package com.focalsolution.webservice.ServiceStorage;

/**
 * Created by sithikorn on 8/21/14 AD.
 */
public class ServiceStoreEntity {
    public String serviceUrl;
    public String serviceParametor;
    public String serviceResult;

    public ServiceStoreEntity(String serviceUrl, String serviceParametor, String serviceResult) {
        this.serviceUrl = serviceUrl;
        this.serviceParametor = serviceParametor;
        this.serviceResult = serviceResult;
    }
}
