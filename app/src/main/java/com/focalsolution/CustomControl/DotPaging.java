package com.focalsolution.CustomControl;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.focalsolution.utility.Util;

/**
 * Created by sithikorn on 7/29/14 AD.
 */
public class DotPaging {
    int selectedResourceID, unSelectResourceID;
    LinearLayout layoutPaging;

    Gallery imageGallery;
    Context context;
    int lastSelect = -1;

    final int margin, selectWidth, defaultWidth;

    public DotPaging(int selectedResourceID, int unSelectResourceID, LinearLayout _layoutPaging, Gallery imageGallery) {
        this.selectedResourceID = selectedResourceID;
        this.unSelectResourceID = unSelectResourceID;
        this.layoutPaging = _layoutPaging;
        this.imageGallery = imageGallery;
        this.context = imageGallery.getContext();
        lastSelect = -1;

        defaultWidth = (int) Util.convertDiptoPix(5, context.getResources());
        selectWidth = (int) Util.convertDiptoPix(8, context.getResources());
        margin = (int) Util.convertDiptoPix(2, context.getResources());

        //init();

        imageGallery.getAdapter().registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                init();
            }
        });
    }

    void init()
    {
        layoutPaging.removeAllViews();

        for (int i = 0;i<imageGallery.getAdapter().getCount() && imageGallery.getAdapter().getCount() > 1;i++)
        {
            final ImageView a = new ImageView(context);

            if(i==0)
            {
                LinearLayout.LayoutParams para = new LinearLayout.LayoutParams(selectWidth, selectWidth);
                para.setMargins(margin, 0, margin, 0);
                a.setLayoutParams(para);
                a.setImageDrawable(context.getResources().getDrawable(selectedResourceID));
            }
            else
            {
                LinearLayout.LayoutParams para = new LinearLayout.LayoutParams(defaultWidth, defaultWidth);
                para.setMargins(margin, 0, margin, 0);
                a.setLayoutParams(para);
                a.setImageDrawable(context.getResources().getDrawable(unSelectResourceID));
            }

            layoutPaging.addView(a);
        }

        final AdapterView.OnItemSelectedListener delegate = imageGallery.getOnItemSelectedListener();
        imageGallery.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                if (layoutPaging.getChildCount() > 1) {
                    final ImageView current = (ImageView) layoutPaging.getChildAt(position);
                    current.setImageDrawable(context.getResources().getDrawable(selectedResourceID));
                    LinearLayout.LayoutParams para = new LinearLayout.LayoutParams(selectWidth, selectWidth);
                    para.setMargins(margin, 0, margin, 0);
                    current.setLayoutParams(para);

                    if (lastSelect >= 0) {
                        final ImageView last = (ImageView) layoutPaging.getChildAt(lastSelect);
                        last.setImageDrawable(context.getResources().getDrawable(unSelectResourceID));
                        para = new LinearLayout.LayoutParams(defaultWidth, defaultWidth);
                        para.setMargins(margin, 0, margin, 0);
                        last.setLayoutParams(para);
                    }

                    lastSelect = position;
                }

                if(delegate != null) delegate.onItemSelected(adapterView, view, position, l);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                if(delegate != null) delegate.onNothingSelected(adapterView);
            }
        });
    }
}
