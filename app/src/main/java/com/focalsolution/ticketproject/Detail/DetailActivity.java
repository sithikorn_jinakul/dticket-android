package com.focalsolution.ticketproject.Detail;

import android.app.ActionBar;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import com.focalsolution.CustomControl.DotPaging;
import com.focalsolution.ticketproject.R;
import com.focalsolution.ticketproject.Setting;
import com.focalsolution.utility.Util;
import com.focalsolution.webservice.Entity.EventDetail;
import com.focalsolution.webservice.Entity.EventList;
import com.focalsolution.webservice.Entity.Photo;
import com.focalsolution.webservice.Entity.TicketType;
import com.focalsolution.webservice.ServiceManager;

import java.text.DecimalFormat;
import java.util.List;

/**
 * Created by sithikorn on 9/23/14 AD.
 */
public class DetailActivity extends Activity {

    RelativeLayout ticket_detail_mainlayout;
    LinearLayout ticket_detail_loading;

    Gallery ticket_detail_gallery;
    LinearLayout ticket_detail_layout_paging;
    com.focalsolution.ticketproject.Adapter.DetailGallery.Adapter adapter;
    TextView ticket_detail_tv_header;

    ImageView ticket_detail_iv_over;

    ViewSwitcher ticket_detail_switcher;

    Activity currentActivity;

    int eventID;

    //region show detail
    TextView ticket_detail_tv_name, ticket_detail_tv_date;
    ImageButton ticket_detail_ib_share;

    TextView ticket_detail_tv_price_text, ticket_detail_tv_price;

    TextView ticket_detail_tv_who_text;
    LinearLayout ticket_detail_layout_who;

    Button ticket_detail_bt_booknow;
    double ticketPrice = 0;

    ViewSwitcher ticket_detail_viewswitcher_price;
    Gallery ticket_detail_gallery_num_price;
    com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Adapter adapterPrice;
    //endregion

    //region confirm book
    TextView ticket_detail_tv_num_ticket_text;
    ImageButton ticket_detail_ib_num_ticket_remove, ticket_detail_ib_num_ticket_add;
    Gallery ticket_detail_gallery_num_ticket;
    com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Adapter adapterNumber;

    TextView ticket_detail_tv_ticket_price_text,ticket_detail_tv_ticket_price;
    TextView ticket_detail_tv_ticket_total_price,ticket_detail_tv_ticket_total_price_text;

    Button ticket_detail_bt_confirmbook;
    //endregion

    private EventDetail eventDetail;
    private com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Entity selectedTicket;

    ProgressDialog loadingDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ticket_detail);

        currentActivity = this;

        eventID = getIntent().getIntExtra("event_id", 0);

        loadingDialog = ProgressDialog.show(currentActivity, "Loading",
                "Sending data to server...", false, false);
        loadingDialog.dismiss();

        setLayout();

        setEvent();

        loadData();
    }

    private void setLayout() {

        ticket_detail_mainlayout = (RelativeLayout) findViewById(R.id.ticket_detail_mainlayout);
        ticket_detail_loading = (LinearLayout) findViewById(R.id.ticket_detail_loading);

        ticket_detail_gallery = (Gallery) findViewById(R.id.ticket_detail_gallery);
        adapter = new com.focalsolution.ticketproject.Adapter.DetailGallery.Adapter(getApplicationContext());
        ticket_detail_gallery.setAdapter(adapter);

        ticket_detail_layout_paging = (LinearLayout) findViewById(R.id.ticket_detail_layout_paging);

        ticket_detail_tv_header = (TextView) findViewById(R.id.ticket_detail_tv_header);

        ticket_detail_iv_over = (ImageView) findViewById(R.id.ticket_detail_iv_over);

        ticket_detail_switcher = (ViewSwitcher) findViewById(R.id.ticket_detail_switcher);

        ActionBar ab = getActionBar();
        if(ab!=null)
        {
            if(ab.isShowing())
                ab.hide();
        }

        //region show detail
        ticket_detail_tv_name= (TextView) findViewById(R.id.ticket_detail_tv_name);
        ticket_detail_tv_date = (TextView) findViewById(R.id.ticket_detail_tv_date);
        ticket_detail_ib_share = (ImageButton) findViewById(R.id.ticket_detail_ib_share);

        ticket_detail_tv_price_text = (TextView) findViewById(R.id.ticket_detail_tv_price_text);
        ticket_detail_tv_price = (TextView) findViewById(R.id.ticket_detail_tv_price);

        ticket_detail_tv_who_text = (TextView) findViewById(R.id.ticket_detail_tv_who_text);
        ticket_detail_layout_who = (LinearLayout) findViewById(R.id.ticket_detail_layout_who);

        ticket_detail_bt_booknow = (Button) findViewById(R.id.ticket_detail_bt_booknow);

        ticket_detail_viewswitcher_price = (ViewSwitcher) findViewById(R.id.ticket_detail_viewswitcher_price);
        ticket_detail_gallery_num_price = (Gallery) findViewById(R.id.ticket_detail_gallery_num_price);

        adapterPrice =new com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Adapter(getApplicationContext(), R.color.app_color_green, 20);
        ticket_detail_gallery_num_price.setAdapter(adapterPrice);
        //endregion

        //region confirm book
        ticket_detail_tv_num_ticket_text = (TextView) findViewById(R.id.ticket_detail_tv_num_ticket_text);
        ticket_detail_ib_num_ticket_remove = (ImageButton) findViewById(R.id.ticket_detail_ib_num_ticket_remove);
        ticket_detail_ib_num_ticket_add = (ImageButton) findViewById(R.id.ticket_detail_ib_num_ticket_add);
        ticket_detail_gallery_num_ticket = (Gallery) findViewById(R.id.ticket_detail_gallery_num_ticket);

        adapterNumber =new com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Adapter(getApplicationContext(), R.color.app_color_black, 28);
        ticket_detail_gallery_num_ticket.setAdapter(adapterNumber);

        ticket_detail_tv_ticket_price_text = (TextView) findViewById(R.id.ticket_detail_tv_ticket_price_text);
        ticket_detail_tv_ticket_price = (TextView) findViewById(R.id.ticket_detail_tv_ticket_price);
        ticket_detail_tv_ticket_total_price = (TextView) findViewById(R.id.ticket_detail_tv_ticket_total_price);
        ticket_detail_tv_ticket_total_price_text = (TextView) findViewById(R.id.ticket_detail_tv_ticket_total_price_text);

        ticket_detail_bt_confirmbook = (Button) findViewById(R.id.ticket_detail_bt_confirmbook);
        //endregion
    }

    private void setEvent() {
        ticket_detail_bt_booknow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ticket_detail_switcher.showNext();
                ticket_detail_iv_over.setVisibility(View.VISIBLE);

                if(eventDetail.ticket_types.size() > 1) {
                    ticket_detail_gallery_num_ticket.setSelection(0);
                    DecimalFormat df = new DecimalFormat("#,##0.##");
                    ticket_detail_tv_ticket_total_price.setText("฿ " + df.format( ticketPrice));
                    ticket_detail_tv_ticket_price.setText("฿ " + df.format(ticketPrice));

                    if (eventDetail.ticket_types.size() > 0) {
                        loadMaxTicketNumber(eventDetail.ticket_types.get(ticket_detail_gallery_num_price.getSelectedItemPosition()).number_of_tickets_remain);
                    }
                } else {
                    DecimalFormat df = new DecimalFormat("#,##0.##");
                    ticket_detail_tv_ticket_total_price.setText("฿ " + df.format(ticketPrice));
                    ticket_detail_tv_ticket_price.setText("฿ " + df.format(ticketPrice));

                    if (eventDetail.ticket_types.size() > 0) {
                        loadMaxTicketNumber(eventDetail.ticket_types.get(0).number_of_tickets_remain);
                        selectedTicket = new com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Entity(eventDetail.ticket_types.get(0).id+"", eventDetail.ticket_types.get(0).price+"");
                    }
                }
            }
        });

        ticket_detail_ib_num_ticket_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ticket_detail_gallery_num_ticket.getSelectedItemPosition() + 1 < adapterNumber.getCount()) {
                    ticket_detail_gallery_num_ticket.setSelection(ticket_detail_gallery_num_ticket.getSelectedItemPosition() + 1, true);
                }
            }
        });

        ticket_detail_ib_num_ticket_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ticket_detail_gallery_num_ticket.getSelectedItemPosition() - 1 >= 0) {
                    ticket_detail_gallery_num_ticket.setSelection(ticket_detail_gallery_num_ticket.getSelectedItemPosition() - 1, true);
                }
            }
        });

        ticket_detail_gallery_num_ticket.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                DecimalFormat df = new DecimalFormat("#,##0.##");
                String totalPrice = df.format((position +1) * ticketPrice);

                ticket_detail_tv_ticket_total_price.setText("฿ " + totalPrice);


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        new DotPaging(R.drawable.page_select, R.drawable.page_none, ticket_detail_layout_paging, ticket_detail_gallery);

        ticket_detail_iv_over.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        ticket_detail_gallery_num_price.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                selectedTicket = adapterPrice.getItem(position);
                ticketPrice = Util.toDouble(selectedTicket.value.replace("฿ ", "").replace(",",""), 0f);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ticket_detail_bt_confirmbook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(selectedTicket!=null) {
                    new Thread(SendBooking).start();
                }
            }
        });
    }

    private void loadData() {
        new Thread(GetDataRunnable).start();
/*
        String title = "bodyslam\nปรากฎการณ์ ดัม-มะ-ชา-ติ";
        ticket_detail_tv_header.setText(title);
        ticket_detail_tv_name.setText(title);
        ticket_detail_tv_name.setSelected(true);

        ticket_detail_tv_date.setText("Monday, October 12, 2014");
        adapter.addItem(new com.focalsolution.ticketproject.Adapter.DetailGallery.Entity("http://www.igossipy.com/wp-content/uploads/2014/08/5d25776e.jpg"));
        adapter.addItem(new com.focalsolution.ticketproject.Adapter.DetailGallery.Entity("http://www.thai2music.com/upload/other/_temp/20140805155758VGou0dXTQw.jpg"));
        adapter.addItem(new com.focalsolution.ticketproject.Adapter.DetailGallery.Entity("http://www.igossipy.com/wp-content/uploads/2014/08/c8c98941.jpg"));
        adapter.addItem(new com.focalsolution.ticketproject.Adapter.DetailGallery.Entity("http://scontent-a.cdninstagram.com/hphotos-xap1/t51.2885-15/926520_596022120518174_501776175_n.jpg"));
        adapter.addItem(new com.focalsolution.ticketproject.Adapter.DetailGallery.Entity("http://www.nanalady.com/picture/2014/08/1409417291.jpg"));

        adapter.notifyDataSetChanged();

        DecimalFormat df = new DecimalFormat("#,##0.##");
        String totalPrice = df.format(ticketPrice);
        ticket_detail_tv_price.setText("฿ " + totalPrice);

        for(int i=1;i<=99;i++)
        {
            adapterNumber.addItem(String.format("%02d",i));
        }
        adapterNumber.notifyDataSetChanged();

        adapterPrice.addItem("฿ " + df.format(1500));
        adapterPrice.addItem("฿ " + df.format(2000));
        adapterPrice.addItem("฿ " + df.format(3000));
        adapterPrice.addItem("฿ " + df.format(4000));
        adapterPrice.addItem("฿ " + df.format(6000));

        if(adapterPrice.getCount() > 1)
        {
            ticket_detail_viewswitcher_price.showNext();
            adapterPrice.notifyDataSetChanged();
        }
        */
    }

    @Override
    public void onBackPressed() {
        if(ticket_detail_iv_over.getVisibility() == View.VISIBLE)
        {
            ticket_detail_switcher.showNext();
            ticket_detail_iv_over.setVisibility(View.INVISIBLE);
        } else {
            super.onBackPressed();
        }
    }

    private void loadMaxTicketNumber(final int max)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                adapterNumber.clearAllItem();
                for(int i=1;i<=max;i++)
                {
                    adapterNumber.addItem(new com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Entity(i + "", String.format("%02d",i)));
                }
                adapterNumber.notifyDataSetChanged();
            }
        });
    }
    private void setData(final EventDetail data)
    {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String title = data.name;
                ticket_detail_tv_header.setText(title);
                ticket_detail_tv_name.setText(title);
                ticket_detail_tv_name.setSelected(true);

                ticket_detail_tv_date.setText(data.getStartTime("EEEE, MMMM dd, yyyy"));

                for (Photo photo : data.photos)
                {
                    adapter.addItem(new com.focalsolution.ticketproject.Adapter.DetailGallery.Entity(photo.url));
                }
                adapter.notifyDataSetChanged();

                if(data.ticket_types.size() > 1)
                {
                    ticket_detail_viewswitcher_price.showNext();
                    DecimalFormat df = new DecimalFormat("#,##0.##");
                    for (TicketType t : data.ticket_types)
                    {
                        adapterPrice.addItem(new com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Entity(t.id + "", "฿ " + df.format(t.price)));
                    }
                    adapterPrice.notifyDataSetChanged();

                } else {

                    ticket_detail_tv_price.setText("฿ " + 0);

                    if(data.ticket_types != null) {
                        if(data.ticket_types.size() > 0) {
                            ticketPrice = data.ticket_types.get(0).price;

                            DecimalFormat df = new DecimalFormat("#,##0.##");
                            String totalPrice = df.format(ticketPrice);
                            ticket_detail_tv_price.setText("฿ " + totalPrice);
                        }
                    }
                }

                ticket_detail_mainlayout.setVisibility(View.VISIBLE);
                ticket_detail_loading.setVisibility(View.GONE);
            }
        });
    }

    Runnable SendBooking = new Runnable() {
        @Override
        public void run() {

            currentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });

            if (Util.isOnline(currentActivity)) {
                ServiceManager s = new ServiceManager(
                        currentActivity
                        , Setting.getDeviceId(currentActivity)
                        , Setting.getManufacturer()
                        , Setting.getModel()
                        , Setting.getOsVersion());

                int ticketID = Util.toInterger(selectedTicket.key, 0);
                com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Entity n = (com.focalsolution.ticketproject.Adapter.GalleryNumPickup.Entity)ticket_detail_gallery_num_ticket.getSelectedItem();
                int numberOfTicket = Util.toInterger(n.key, 0);
                boolean flg = s.bookingTicket(eventID, ticketID, numberOfTicket, Setting.getUserID());

                if(flg)
                {
                    currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            loadingDialog.dismiss();
                            Toast.makeText(currentActivity, "Booking complete.", Toast.LENGTH_LONG).show();
                            currentActivity.finish();
                        }
                    });
                }
            }else {
                currentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(currentActivity, "Cannot connect to Server!", Toast.LENGTH_LONG).show();
                        loadingDialog.dismiss();
                    }
                });
            }
        }
    };

    Runnable GetDataRunnable = new Runnable() {
        @Override
        public void run() {
            currentActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ticket_detail_loading.setVisibility(View.VISIBLE);
                    ticket_detail_mainlayout.setVisibility(View.GONE);
                }
            });

            if(Util.isOnline(currentActivity)) {

                ServiceManager s = new ServiceManager(
                        currentActivity
                        , Setting.getDeviceId(currentActivity)
                        , Setting.getManufacturer()
                        , Setting.getModel()
                        , Setting.getOsVersion());

                eventDetail = s.getEvent(eventID);

                if(eventDetail != null)
                {
                    setData(eventDetail);
                } else {
                    currentActivity.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ticket_detail_loading.setVisibility(View.GONE);
                            ticket_detail_mainlayout.setVisibility(View.VISIBLE);
                        }
                    });
                }

            } else {
                currentActivity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(currentActivity, "Cannot connect to Server!", Toast.LENGTH_LONG).show();
                        ticket_detail_loading.setVisibility(View.GONE);
                    }
                });
            }
        }
    };
}
