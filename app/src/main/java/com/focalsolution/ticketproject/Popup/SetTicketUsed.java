package com.focalsolution.ticketproject.Popup;

import android.app.Dialog;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.focalsolution.ticketproject.Fragment.MyTicketFragment;
import com.focalsolution.ticketproject.R;
import com.focalsolution.utility.Util;

import java.text.DecimalFormat;

/**
 * Created by sithikorn on 9/26/14 AD.
 */
public class SetTicketUsed {

    Dialog dialogBox;

    final MyTicketFragment currentActivity;

    double eventLat, eventLng;
    Location userLocation;
    String bookingCode;
    double distance;

    TextView popup_used_bookingid;
    TextView popup_used_eventlocation;
    TextView popup_used_userlocation;
    TextView popup_used_distance;
    TextView popup_used_accuracy;
    EditText popup_used_requiresnearby;

    TextView popup_used_result;

    Button popup_used_bt_no,popup_used_bt_yes;

    public SetTicketUsed(MyTicketFragment currentActivity, String bookingCode, double eLat, double eLng) {
        this.currentActivity = currentActivity;

        dialogBox = new Dialog(currentActivity.getActivity());

        this.eventLat = eLat;
        this.eventLng = eLng;
        this.bookingCode = bookingCode;

        setLayout();

        loadData();

        setEvent();

        dialogBox.show();
    }

    private void setEvent() {

        currentActivity.setOnLocationChange(new MyTicketFragment.OnLocationChange() {
            @Override
            public void onUserLocationChange(Location location) {
                userLocation = location;

                displayData();
            }
        });

        popup_used_bt_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogBox.dismiss();
            }
        });

        final Handler keyChange = new Handler();
        popup_used_requiresnearby.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                keyChange.removeCallbacks(onTextChangeRunnable);
                keyChange.postDelayed(onTextChangeRunnable, 200);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    Runnable onTextChangeRunnable = new Runnable() {
        @Override
        public void run() {
            displayData();
        }
    };

    private void loadData() {

        userLocation = currentActivity.getBestLocation();
        popup_used_bookingid.setText(bookingCode);

        displayData();
    }

    private void displayData()
    {
        popup_used_userlocation.setText(userLocation.getLatitude() + ", " + userLocation.getLongitude());

        popup_used_eventlocation.setText(eventLat + ", " + eventLng);

        distance = Util.calDistance(userLocation.getLatitude()
                , userLocation.getLongitude()
                , eventLat
                , eventLng
                , 'K') * 1000;

        DecimalFormat df = new DecimalFormat("#,##0.00");
        popup_used_distance.setText((df.format(distance)) + " meter");
        popup_used_accuracy.setText(userLocation.getAccuracy() +"");

        double setNearBy = Util.toDouble(popup_used_requiresnearby.getText().toString(), 0f);

        if(distance <= setNearBy)
        {
            popup_used_result.setText("Would you like to use this Ticket?");
            popup_used_bt_yes.setEnabled(true);
        } else {
            popup_used_result.setText("Your location not near Event location.");
            popup_used_bt_yes.setEnabled(false);
        }
    }

    public void dismiss()
    {
        dialogBox.dismiss();
    }

    private void setLayout() {
        dialogBox.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogBox.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialogBox.setCancelable(false);
        dialogBox.setContentView(R.layout.popup_setused);

        popup_used_bookingid = (TextView) dialogBox.findViewById(R.id.popup_used_bookingid);
        popup_used_eventlocation = (TextView) dialogBox.findViewById(R.id.popup_used_eventlocation);
        popup_used_userlocation = (TextView) dialogBox.findViewById(R.id.popup_used_userlocation);
        popup_used_distance = (TextView) dialogBox.findViewById(R.id.popup_used_distance);
        popup_used_accuracy = (TextView) dialogBox.findViewById(R.id.popup_used_accuracy);

        popup_used_requiresnearby = (EditText) dialogBox.findViewById(R.id.popup_used_requiresnearby);

        popup_used_bt_no = (Button) dialogBox.findViewById(R.id.popup_used_bt_no);
        popup_used_bt_yes = (Button) dialogBox.findViewById(R.id.popup_used_bt_yes);

        popup_used_result = (TextView) dialogBox.findViewById(R.id.popup_used_result);
    }

    public void setOnYesClick(View.OnClickListener l)
    {
        popup_used_bt_yes.setOnClickListener(l);
    }
}
