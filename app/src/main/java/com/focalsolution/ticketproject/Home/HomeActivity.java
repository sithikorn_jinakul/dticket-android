package com.focalsolution.ticketproject.Home;

import android.app.Activity;

import android.app.ActionBar;
import android.app.FragmentManager;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.ViewSwitcher;

import com.focalsolution.ticketproject.Fragment.HomeFragment;
import com.focalsolution.ticketproject.Fragment.MyTicketFragment;
import com.focalsolution.ticketproject.R;


public class HomeActivity extends Activity
        implements NavigationDrawerFragment.NavigationDrawerCallbacks {

    /**
     * Fragment managing the behaviors, interactions and presentation of the navigation drawer.
     */
    private NavigationDrawerFragment mNavigationDrawerFragment;

    /**
     * Used to store the last screen title. For use in {@link #setActionBarDropdown()}.
     */
    private DrawerLayout drawer_layout;
    private int currentMenuSelected = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getWindow().requestFeature(Window.FEATURE_ACTION_BAR_OVERLAY);

        setContentView(R.layout.activity_main);

        mNavigationDrawerFragment = (NavigationDrawerFragment)
                getFragmentManager().findFragmentById(R.id.navigation_drawer);

        // Set up the drawer.
        drawer_layout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mNavigationDrawerFragment.setUp(
                R.id.navigation_drawer,
                drawer_layout);
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getFragmentManager();
        switch (position)
        {
            case 0 :
            fragmentManager.beginTransaction()
                    .replace(R.id.container, HomeFragment.newInstance(position + 1), "HomeFragment")
                    .commit();
                break;
            case 1 :
                fragmentManager.beginTransaction()
                        .replace(R.id.container, MyTicketFragment.newInstance(position + 1), "MyTicketFragment")
                        .commit();
                //com.focalsolution.ticketproject.Adapter.Product.Entity data = adapter.getItem(position);
                /*Intent i = new Intent("com.focalsolution.ticketproject.Detail.DetailActivity");
                i.putExtra("event_id", 0);
                startActivity(i);*/
                break;
        }
        currentMenuSelected = position;
    }

    public void onSectionAttached(int number) {
        //mTitle = getResources().getStringArray(R.array.app_menu_left)[number-1];
    }

    public void setActionBarDropdown() {
        final ActionBar actionBar = getActionBar();
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.view_header_spinner);

        ImageButton view_header_menu = (ImageButton) findViewById(R.id.view_header_menu);
        Spinner view_header_spinner = (Spinner) findViewById(R.id.view_header_spinner);
        // Create an ArrayAdapter using the string array and a default spinner layout
        final ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.app_menu_dropdown, android.R.layout.simple_spinner_item);
        // Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // Apply the adapter to the spinner
        view_header_spinner.setAdapter(adapter);
        actionBar.show();
        setActionBarEvent(view_header_menu, view_header_spinner);
    }

    private void setActionBarText() {
        final ActionBar actionBar = getActionBar();

        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        actionBar.setCustomView(R.layout.view_header_spinner);
        actionBar.getCustomView().setBackgroundColor(getResources().getColor(R.color.app_color_dark));

        ImageButton view_header_menu = (ImageButton) findViewById(R.id.view_header_menu);
        ImageButton view_header_search = (ImageButton) findViewById(R.id.view_header_search);
        view_header_menu.setImageDrawable(getResources().getDrawable(R.drawable.icon_menu_white));
        view_header_search.setImageDrawable(getResources().getDrawable(R.drawable.icon_search_white));

        ((ViewSwitcher) findViewById(R.id.view_header_viewswitcher)).showNext();
        TextView view_header_tv = (TextView) findViewById(R.id.view_header_tv);
        view_header_tv.setText(MyTicketFragment.titleList.replace('\n', ' '));
        view_header_tv.setTextColor(getResources().getColor(R.color.app_color_white));

        //actionBar.hide();
        setActionBarEvent(view_header_menu, null);

    }

    private void setActionBarEvent(ImageButton view_header_menu, Spinner view_header_spinner)
    {
        view_header_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawer_layout.openDrawer(Gravity.LEFT);
            }
        });

        if(view_header_spinner != null) {
            view_header_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    FragmentManager fragmentManager = getFragmentManager();
                    HomeFragment homeFragment = (HomeFragment) fragmentManager.findFragmentByTag("HomeFragment");
                    if (homeFragment != null) {
                        homeFragment.onDropDownChange(position);
                    }
                /*switch (position)
                {
                    case 0 :
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, HomeFragment.newInstance(position + 1))
                                .commit();
                        break;
                    case 1 :
                        fragmentManager.beginTransaction()
                                .replace(R.id.container, EventFragment.newInstance(position + 1))
                                .commit();
                        break;
                }*/
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if (!mNavigationDrawerFragment.isDrawerOpen() && currentMenuSelected == 0) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            setActionBarDropdown();
            return true;
        } else if (!mNavigationDrawerFragment.isDrawerOpen()) {
            setActionBarText();
            return true;
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        //currentMenuSelected = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
