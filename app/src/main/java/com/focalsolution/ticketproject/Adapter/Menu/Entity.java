package com.focalsolution.ticketproject.Adapter.Menu;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class Entity {
    public String name;
    public int icon;

    public Entity(String name, int icon) {
        this.name = name;
        this.icon = icon;
    }
}
