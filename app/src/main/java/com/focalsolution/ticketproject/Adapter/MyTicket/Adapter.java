package com.focalsolution.ticketproject.Adapter.MyTicket;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.focalsolution.ticketproject.R;
import com.focalsolution.ticketproject.Setting;
import com.focalsolution.utility.ImageDownloader;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class Adapter extends BaseAdapter {

    Context currentContext;
    List<Entity> itemList;
    ImageDownloader imageDownloader;
    public Adapter(Context c) {
        this.currentContext = c;
        this.itemList = new LinkedList<Entity>();

        imageDownloader = new ImageDownloader(currentContext,
                Setting.getDefaultCacheDir(currentContext),
                BitmapFactory.decodeResource(currentContext.getResources(),
                        R.drawable.noimage_dark));
        //,(AnimationDrawable)currentContext.getResources().getDrawable(R.drawable.noimage_light)
        imageDownloader.setDefaultColor(currentContext.getResources().getColor(R.color.app_color_black));
        imageDownloader.setDownloadAnimScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageDownloader.setExpires(0, 1, 25);
    }

    public void addItem(Entity item)
    {
        itemList.add(item);
    }

    public void removeItem(int position)
    {
        itemList.remove(position);
    }

    public void clearAllItem()
    {
        itemList.clear();
    }
    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Entity getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public boolean isEnabled(int position) {
        return !getItem(position).isUsed;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        Entity info = getItem(position);

        if (convertView == null)
        {
            convertView = LayoutInflater.from(currentContext).inflate(R.layout.listitem_myticket, parent, false);
            holder = new ViewHolder();

            holder.listitem_myticket_iv_image = (ImageView) convertView.findViewById(R.id.listitem_myticket_iv_image);

            holder.listitem_myticket_tv_name = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_name);
            holder.listitem_myticket_tv_date = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_date);
            holder.listitem_myticket_tv_year = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_year);

            holder.listitem_myticket_tv_hall_text = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_hall_text);
            holder.listitem_myticket_tv_hall = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_hall);

            holder.listitem_myticket_tv_row_text = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_row_text);
            holder.listitem_myticket_tv_row = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_row);

            holder.listitem_myticket_tv_seat_text = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_seat_text);
            holder.listitem_myticket_tv_seat = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_seat);

            holder.listitem_myticket_tv_location_text = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_location_text);
            holder.listitem_myticket_tv_location = (TextView) convertView.findViewById(R.id.listitem_myticket_tv_location);

            holder.listitem_myticket_iv_barcode = (ImageView) convertView.findViewById(R.id.listitem_myticket_iv_barcode);

            holder.listitem_myticket_layout_used = (LinearLayout) convertView.findViewById(R.id.listitem_myticket_layout_used);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.listitem_myticket_tv_name.setText(info.name);

        holder.listitem_myticket_tv_date.setText(info.date);
        holder.listitem_myticket_tv_year.setText(", " + info.year);

        holder.listitem_myticket_tv_hall.setText(info.hall);
        holder.listitem_myticket_tv_row.setText(info.row);
        holder.listitem_myticket_tv_seat.setText(info.seat);

        holder.listitem_myticket_tv_location.setText(info.location);

        imageDownloader.download(info.coverImage, holder.listitem_myticket_iv_image);

        imageDownloader.download(info.barcodeImageUrl, holder.listitem_myticket_iv_barcode);

        holder.listitem_myticket_layout_used.setVisibility(info.isUsed ? View.VISIBLE : View.GONE);

        return convertView;
    }

    public static class ViewHolder {
        ImageView listitem_myticket_iv_image;
        TextView listitem_myticket_tv_name, listitem_myticket_tv_date, listitem_myticket_tv_year;
        TextView listitem_myticket_tv_hall_text, listitem_myticket_tv_hall;
        TextView listitem_myticket_tv_row_text, listitem_myticket_tv_row;
        TextView listitem_myticket_tv_seat_text, listitem_myticket_tv_seat;
        TextView listitem_myticket_tv_location_text, listitem_myticket_tv_location;

        ImageView listitem_myticket_iv_barcode;

        LinearLayout listitem_myticket_layout_used;
    }
}
