package com.focalsolution.ticketproject.Adapter.Product;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.drawable.AnimationDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.focalsolution.ticketproject.R;
import com.focalsolution.ticketproject.Setting;
import com.focalsolution.utility.ImageDownloader;

import java.text.DecimalFormat;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class Adapter extends BaseAdapter {

    Context currentContext;
    List<Entity> itemList;
    ImageDownloader imageDownloader;
    public Adapter(Context c) {
        this.currentContext = c;
        this.itemList = new LinkedList<Entity>();

        imageDownloader = new ImageDownloader(currentContext,
                Setting.getDefaultCacheDir(currentContext),
                BitmapFactory.decodeResource(currentContext.getResources(),
                        R.drawable.noimage_dark));
        //,(AnimationDrawable)currentContext.getResources().getDrawable(R.drawable.noimage_light)
        imageDownloader.setDefaultColor(currentContext.getResources().getColor(R.color.app_default_image_loading));
        imageDownloader.setDownloadAnimScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageDownloader.setExpires(0, 1, 25);
    }

    public void addItem(Entity item)
    {
        itemList.add(item);
    }

    public void removeItem(int position)
    {
        itemList.remove(position);
    }

    public void clearAllItem()
    {
        itemList.clear();
    }
    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Entity getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        Entity info = getItem(position);

        if (convertView == null)
        {
            convertView = LayoutInflater.from(currentContext).inflate(R.layout.listitem_product, parent, false);
            holder = new ViewHolder();

            holder.listitem_product_iv_cover = (ImageView) convertView.findViewById(R.id.listitem_product_iv_cover);

            holder.listitem_product_tv_name = (TextView) convertView.findViewById(R.id.listitem_product_tv_name);
            holder.listitem_product_tv_date = (TextView) convertView.findViewById(R.id.listitem_product_tv_date);

            holder.listitem_product_tv_price = (TextView) convertView.findViewById(R.id.listitem_product_tv_price);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.listitem_product_tv_name.setText(info.name);
        holder.listitem_product_tv_date.setText(info.date);

        if(info.price > 0.0f) {
            DecimalFormat df = new DecimalFormat("#,##0.##");
            holder.listitem_product_tv_price.setText("฿ " + df.format(info.price));
            holder.listitem_product_tv_price.setVisibility(View.VISIBLE);
        } else {
            holder.listitem_product_tv_price.setVisibility(View.INVISIBLE);
        }

        imageDownloader.download(info.iconCover, holder.listitem_product_iv_cover);

        return convertView;
    }

    public static class ViewHolder {
        ImageView listitem_product_iv_cover;
        TextView listitem_product_tv_name, listitem_product_tv_date;
        TextView listitem_product_tv_price;
    }
}
