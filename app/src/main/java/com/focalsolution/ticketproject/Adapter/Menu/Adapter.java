package com.focalsolution.ticketproject.Adapter.Menu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.focalsolution.ticketproject.R;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class Adapter extends BaseAdapter {

    Context currentContext;
    List<Entity> itemList;
    public Adapter(Context c) {
        this.currentContext = c;
        this.itemList = new LinkedList<Entity>();
    }

    public void addItem(Entity item)
    {
        itemList.add(item);
    }

    public void removeItem(int position)
    {
        itemList.remove(position);
    }

    public void clearAllItem()
    {
        itemList.clear();
    }
    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Entity getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        Entity info = getItem(position);

        if (convertView == null)
        {
            convertView = LayoutInflater.from(currentContext).inflate(R.layout.listitem_menu, parent, false);
            holder = new ViewHolder();

            holder.listitem_menu_icon = (ImageView) convertView.findViewById(R.id.listitem_menu_icon);

            holder.listitem_menu_name = (TextView) convertView.findViewById(R.id.listitem_menu_name);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.listitem_menu_name.setText(info.name);
        holder.listitem_menu_icon.setImageResource(info.icon);

        return convertView;
    }

    public static class ViewHolder {
        ImageView listitem_menu_icon;
        TextView listitem_menu_name;
    }
}
