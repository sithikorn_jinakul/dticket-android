package com.focalsolution.ticketproject.Adapter.MyTicket;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class Entity {
    public int id;
    public String name;
    public String date;
    public String year;
    public String hall, row, seat;
    public String location;
    public double lat, lng;
    public String barcodeImageUrl;
    public String code;
    public String coverImage;

    public boolean isUsed = false;

    public Entity(String coverImage, String name, String date, String year, String hall, String row, String seat, String location, String barcodeImageUrl) {
        this.coverImage = coverImage;
        this.name = name;
        this.date = date;
        this.year = year;
        this.hall = hall;
        this.row = row;
        this.seat = seat;
        this.location = location;
        this.barcodeImageUrl = barcodeImageUrl;
    }

    public Entity() {
    }

    public Entity setUsed(boolean isUsed) {
        this.isUsed = isUsed;
        return this;
    }
}
