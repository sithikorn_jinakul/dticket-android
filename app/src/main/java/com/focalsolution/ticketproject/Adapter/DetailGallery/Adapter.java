package com.focalsolution.ticketproject.Adapter.DetailGallery;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.focalsolution.ticketproject.R;
import com.focalsolution.ticketproject.Setting;
import com.focalsolution.utility.ImageDownloader;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class Adapter extends BaseAdapter {

    Context currentContext;
    List<Entity> itemList;
    ImageDownloader imageDownloader;

    public Adapter(Context c) {
        this.currentContext = c;
        this.itemList = new LinkedList<Entity>();

        imageDownloader = new ImageDownloader(currentContext,
                Setting.getDefaultCacheDir(currentContext),
                BitmapFactory.decodeResource(currentContext.getResources(),
                        R.drawable.noimage_dark));
        //,(AnimationDrawable)currentContext.getResources().getDrawable(R.drawable.noimage_light)
        imageDownloader.setDefaultColor(currentContext.getResources().getColor(R.color.app_default_image_loading));
        imageDownloader.setDownloadAnimScaleType(ImageView.ScaleType.CENTER_INSIDE);
        imageDownloader.setExpires(0, 1, 25);
    }

    public void addItem(Entity item)
    {
        itemList.add(item);
    }

    public void removeItem(int position)
    {
        itemList.remove(position);
    }

    public void clearAllItem()
    {
        itemList.clear();
    }
    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Entity getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        Entity info = getItem(position);

        if (convertView == null)
        {
            convertView = LayoutInflater.from(currentContext).inflate(R.layout.listitem_gallery_image, parent, false);
            holder = new ViewHolder();

            holder.listitem_gallery_image_iv = (ImageView) convertView.findViewById(R.id.listitem_gallery_image_iv);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        imageDownloader.download(info.icon, holder.listitem_gallery_image_iv);

        return convertView;
    }

    public static class ViewHolder {
        ImageView listitem_gallery_image_iv;
    }
}
