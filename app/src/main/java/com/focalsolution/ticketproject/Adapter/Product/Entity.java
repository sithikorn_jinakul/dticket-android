package com.focalsolution.ticketproject.Adapter.Product;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class Entity {
    public String name;
    public String date;
    public double price;

    public String iconCover;
    public int id;

    public Entity(int _id,String name, String date, double price, String iconCover) {
        this.id = _id;
        this.name = name;
        this.date = date;
        this.price = price;
        this.iconCover = iconCover;
    }

    public Entity() {
    }
}
