package com.focalsolution.ticketproject.Adapter.GalleryNumPickup;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.focalsolution.ticketproject.R;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class Adapter extends BaseAdapter {

    Context currentContext;
    List<Entity> itemList;
    int textColorResourceID, textSize;

    public Adapter(Context c, int textColor, int txtSize) {
        this.currentContext = c;
        this.textColorResourceID = textColor;
        this.textSize = txtSize;
        this.itemList = new LinkedList<Entity>();
    }

    public void addItem(Entity item)
    {
        itemList.add(item);
    }

    public void removeItem(int position)
    {
        itemList.remove(position);
    }

    public void clearAllItem()
    {
        itemList.clear();
    }
    @Override
    public int getCount() {
        return itemList.size();
    }

    @Override
    public Entity getItem(int position) {
        return itemList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder;

        Entity info = getItem(position);

        if (convertView == null)
        {
            convertView = LayoutInflater.from(currentContext).inflate(R.layout.listitem_number_picker, parent, false);
            holder = new ViewHolder();

            holder.listitem_number_picker_tv = (TextView) convertView.findViewById(R.id.listitem_number_picker_tv);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder)convertView.getTag();
        }

        holder.listitem_number_picker_tv.setText(info.value);
        holder.listitem_number_picker_tv.setTextColor(currentContext.getResources().getColor(textColorResourceID));
        holder.listitem_number_picker_tv.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);

        return convertView;
    }

    public static class ViewHolder {
        TextView listitem_number_picker_tv;
    }
}
