package com.focalsolution.ticketproject.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.focalsolution.CustomControl.CircleImageView;
import com.focalsolution.ticketproject.Adapter.MyTicket.Entity;
import com.focalsolution.ticketproject.Home.HomeActivity;
import com.focalsolution.ticketproject.Popup.SetTicketUsed;
import com.focalsolution.ticketproject.R;
import com.focalsolution.ticketproject.Setting;
import com.focalsolution.utility.Util;
import com.focalsolution.webservice.Entity.EventDetail;
import com.focalsolution.webservice.Entity.EventList;
import com.focalsolution.webservice.Entity.Ticket;
import com.focalsolution.webservice.ServiceManager;

import java.util.List;
import java.util.Objects;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class MyTicketFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    public static final String titleList = "Purchased\nTicket";

    CircleImageView myticket_iv_userprofile;
    ListView myticket_listview;
    private View loadingView;
    RelativeLayout myticket_layout_userprofile;
    int paddingTopProfile = 0;
    boolean isScrollIdle = false, showActionBar = true;

    com.focalsolution.ticketproject.Adapter.MyTicket.Adapter adapter;
    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */

    OnLocationChange onLocationChange;

    boolean gps_enabled = false;
    boolean network_enabled = false;
    boolean userlocation_enabled = false;
    LocationManager lm;

    ProgressDialog loadingDialog;

    com.focalsolution.ticketproject.Adapter.MyTicket.Entity selectedTicket;

    public static MyTicketFragment newInstance(int sectionNumber) {
        MyTicketFragment fragment = new MyTicketFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    private android.app.ActionBar getActionBar()
    {
        return getActivity().getActionBar();
    }

    public MyTicketFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_myticket, container, false);

        userlocation_enabled = true;

        if(lm==null)
            lm = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);

        setLayout(rootView);

        setEvent();

        loadData();

        loadingDialog = ProgressDialog.show(getActivity(), "Loading",
                "Sending data to server...", false, false);
        loadingDialog.dismiss();

        return rootView;
    }

    private void loadData() {
        myticket_iv_userprofile.setImageDrawable(getResources().getDrawable(R.drawable.sample_profile));
        new Thread(getMyTicketRunnable).start();
        /*
        String barcodeData = "9786168554135AC";
        adapter.addItem(new com.focalsolution.ticketproject.Adapter.MyTicket.Entity(
                "http://blog.nationmultimedia.com/home/blog_data/380/380/images/RakChab7.jpg"
                , "Bie The Star\nA Lot of Love Concert"
                , "Monday, September 8"
                , "2014"
                , "Hall 1"
                , "A54"
                , "258"
                , "Impact Arena Muang Thong Thani"
                , "http://barcode.tec-it.com/barcode.ashx?code=Code128&modulewidth=fit&data=" + barcodeData + "&dpi=144&imagetype=png&rotation=0&color=&bgcolor=f3f1f2&fontcolor=&quiet=20&qunit=px&download=true"
        ));

        adapter.addItem(new com.focalsolution.ticketproject.Adapter.MyTicket.Entity(
                "http://blog.nationmultimedia.com/home/blog_data/380/380/images/RakChab7.jpg"
                , "Bie The Star\nA Lot of Love Concert"
                , "Monday, September 8"
                , "2014"
                , "Hall 1"
                , "A54"
                , "258"
                , "Impact Arena Muang Thong Thani"
                , "http://barcode.tec-it.com/barcode.ashx?code=Code128&modulewidth=fit&data=" + barcodeData + "&dpi=144&imagetype=png&rotation=0&color=&bgcolor=f3f1f2&fontcolor=&quiet=20&qunit=px&download=true"
        ).setUsed(true));

        adapter.addItem(new com.focalsolution.ticketproject.Adapter.MyTicket.Entity(
                "http://blog.nationmultimedia.com/home/blog_data/380/380/images/RakChab7.jpg"
                , "Bie The Star\nA Lot of Love Concert"
                , "Monday, September 8"
                , "2014"
                , "Hall 1"
                , "A54"
                , "258"
                , "Impact Arena Muang Thong Thani"
                , "http://barcode.tec-it.com/barcode.ashx?code=Code128&modulewidth=fit&data=" + barcodeData + "&dpi=144&imagetype=png&rotation=0&color=&bgcolor=f3f1f2&fontcolor=&quiet=20&qunit=px&download=true"
        ));

        adapter.addItem(new com.focalsolution.ticketproject.Adapter.MyTicket.Entity(
                "http://blog.nationmultimedia.com/home/blog_data/380/380/images/RakChab7.jpg"
                , "Bie The Star\nA Lot of Love Concert"
                , "Monday, September 8"
                , "2014"
                , "Hall 1"
                , "A54"
                , "258"
                , "Impact Arena Muang Thong Thani"
                , "http://barcode.tec-it.com/barcode.ashx?code=Code128&modulewidth=fit&data=" + barcodeData + "&dpi=144&imagetype=png&rotation=0&color=&bgcolor=f3f1f2&fontcolor=&quiet=20&qunit=px&download=true"
        ));

        adapter.addItem(new com.focalsolution.ticketproject.Adapter.MyTicket.Entity(
                "http://blog.nationmultimedia.com/home/blog_data/380/380/images/RakChab7.jpg"
                , "Bie The Star\nA Lot of Love Concert"
                , "Monday, September 8"
                , "2014"
                , "Hall 1"
                , "A54"
                , "258"
                , "Impact Arena Muang Thong Thani"
                , "http://barcode.tec-it.com/barcode.ashx?code=Code128&modulewidth=fit&data=" + barcodeData + "&dpi=144&imagetype=png&rotation=0&color=&bgcolor=f3f1f2&fontcolor=&quiet=20&qunit=px&download=true"
        ));

        adapter.addItem(new Entity(
                "http://blog.nationmultimedia.com/home/blog_data/380/380/images/RakChab7.jpg"
                , "Bie The Star\nA Lot of Love Concert"
                , "Monday, September 8"
                , "2014"
                , "Hall 1"
                , "A54"
                , "258"
                , "Impact Arena Muang Thong Thani"
                , "http://barcode.tec-it.com/barcode.ashx?code=Code128&modulewidth=fit&data=" + barcodeData + "&dpi=144&imagetype=png&rotation=0&color=&bgcolor=f3f1f2&fontcolor=&quiet=20&qunit=px&download=true"
        ));

        adapter.notifyDataSetChanged();
        */
    }

    private void setEvent() {

        final MyTicketFragment myTicketFragment = this;

        //region ListScroll Event to Hide Show ActionBar
        final Handler hideActionBarHandler = new Handler();
        myticket_listview.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState)
                {
                    case SCROLL_STATE_IDLE : isScrollIdle = true;
                        break;
                    default: isScrollIdle = false;
                        hideActionBarHandler.removeCallbacks(hideActionBar);
                        break;
                }
            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if(firstVisibleItem == 0)
                {
                    if(!showActionBar) {
                        /*myticket_layout_userprofile.setPadding(0,paddingTopProfile,0,0);
                        getActionBar().show();
                        hideActionBarHandler.removeCallbacks(hideActionBar);
                        showActionBar = true;
                        */
                        showActionBar(hideActionBarHandler);
                    }
                } else {
                    if(showActionBar) {
                        hideActionBarHandler.removeCallbacks(hideActionBar);
                        hideActionBarHandler.postDelayed(hideActionBar, 200);
                    }
                }
            }
        });

        myticket_listview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                selectedTicket = adapter.getItem(position - 1);

                if(selectedTicket != null) {

                    final SetTicketUsed popup = new SetTicketUsed(myTicketFragment
                            , selectedTicket.code
                            , selectedTicket.lat
                            , selectedTicket.lng);

                    popup.setOnYesClick(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            popup.dismiss();

                            new Thread(setTicketUsedRunnable).start();

                        }
                    });
                }
                return false;
            }
        });
        //endregion

    }

    //region ActionBar Animation Hide Show
    private Runnable hideActionBar = new Runnable() {
        @Override
        public void run() {
            if(isScrollIdle && showActionBar) {
                hideActionBar();
            }
        }
    };

    private void hideActionBar()
    {
        if(getActivity()==null)
            if(getActivity().getActionBar()==null)
                return;

        getActionBar().hide();
        //myticket_layout_userprofile.setPadding(0,0,0,0);
        showActionBar = false;
        Animation a = new Animation() {

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                myticket_layout_userprofile.setPadding(
                        0,
                        (int)(paddingTopProfile - (paddingTopProfile * interpolatedTime)),
                        0,
                        0);
            }
        };
        a.setDuration(300);
        a.setFillAfter(true);
        myticket_layout_userprofile.startAnimation(a);
    }
    private void showActionBar(Handler hideActionBarHandler)
    {
        if(getActivity()==null)
            if(getActivity().getActionBar()==null) {
                hideActionBarHandler.removeCallbacks(hideActionBar
                );
                return;
            }

        myticket_layout_userprofile.setPadding(0,paddingTopProfile,0,0);
        getActionBar().show();
        hideActionBarHandler.removeCallbacks(hideActionBar);
        showActionBar = true;
        Animation a = new Animation() {

            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                myticket_layout_userprofile.setPadding(
                        0,
                        (int)(paddingTopProfile * interpolatedTime),
                        0,
                        0);
            }
        };
        a.setDuration(200);
        a.setFillAfter(true);
        myticket_layout_userprofile.startAnimation(a);
    }
    //endregion

    private void setLayout(View rootView) {
        myticket_iv_userprofile = (CircleImageView) rootView.findViewById(R.id.myticket_iv_userprofile);
        myticket_listview = (ListView) rootView.findViewById(R.id.myticket_listview);

        adapter = new com.focalsolution.ticketproject.Adapter.MyTicket.Adapter(getActivity().getApplicationContext());

        myticket_layout_userprofile = (RelativeLayout) rootView.findViewById(R.id.myticket_layout_userprofile);
        paddingTopProfile = myticket_layout_userprofile.getPaddingTop();

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    @Override
    public void onResume() {
        super.onResume();

        try{gps_enabled=lm.isProviderEnabled(LocationManager.GPS_PROVIDER);}catch(Exception ex){}
        try{network_enabled=lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);}catch(Exception ex){}

        //don't start listeners if no provider is enabled
        if(!gps_enabled && !network_enabled)
        {
            Toast.makeText(getActivity(), "GPS Disable", Toast.LENGTH_LONG).show();
        } else {
            Log.i("BR","Branch listerner");
            if(userlocation_enabled){
                if(gps_enabled)
                    lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListenerGps);
                if(network_enabled)
                    lm.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListenerNetwork);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        lm.removeUpdates(locationListenerNetwork);
        lm.removeUpdates(locationListenerGps);
    }

    LocationListener locationListenerGps = new LocationListener() {
        public void onLocationChanged(Location location) {
            onUserChangeLocation();
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    LocationListener locationListenerNetwork = new LocationListener() {
        public void onLocationChanged(Location location) {
            onUserChangeLocation();
        }
        public void onProviderDisabled(String provider) {}
        public void onProviderEnabled(String provider) {}
        public void onStatusChanged(String provider, int status, Bundle extras) {}
    };

    public void onUserChangeLocation()
    {
        if(userlocation_enabled)
        {
            Location userlocation = Util.getBestLocation(lm);
            if(userlocation != null && onLocationChange != null){
                onLocationChange.onUserLocationChange(userlocation);
            }
        }
    }

    public Location getBestLocation()
    {
        return Util.getBestLocation(lm);
    }

    public OnLocationChange getOnLocationChange() {
        return onLocationChange;
    }

    public void setOnLocationChange(OnLocationChange onLocationChange) {
        this.onLocationChange = onLocationChange;
    }

    public interface OnLocationChange
    {
        public abstract void onUserLocationChange(Location location);
    }

    Runnable setTicketUsedRunnable = new Runnable() {
        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadingDialog.show();
                }
            });

            if(Util.isOnline(getActivity())) {
                ServiceManager s = new ServiceManager(
                        getActivity()
                        , Setting.getDeviceId(getActivity())
                        , Setting.getManufacturer()
                        , Setting.getModel()
                        , Setting.getOsVersion());

                boolean flg = s.setTicketUsed(selectedTicket.id, Setting.getUserID());

                if(flg)
                {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            selectedTicket.setUsed(true);
                            adapter.notifyDataSetChanged();
                            loadingDialog.dismiss();
                            Toast.makeText(getActivity(), "Make ticket used complete.", Toast.LENGTH_LONG).show();
                        }
                    });
                } else {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getActivity(), "Error cannot update data!", Toast.LENGTH_LONG).show();
                            loadingDialog.dismiss();
                        }
                    });
                }

            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Cannot connect to Server!", Toast.LENGTH_LONG).show();
                        loadingDialog.dismiss();
                    }
                });
            }
        }
    };

    Runnable getMyTicketRunnable = new Runnable() {
        @Override
        public void run() {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.clearAllItem();
                    adapter.notifyDataSetChanged();

                    int h = (int)Util.convertDiptoPix(124, getResources());
                    TextView paddingTop = new TextView(getActivity());
                    paddingTop.setHeight(h);

                    h = (int)Util.convertDiptoPix(8, getResources());
                    TextView paddingBottom = new TextView(getActivity());
                    paddingBottom.setHeight(h);

                    loadingView = LayoutInflater.from(getActivity()).inflate(R.layout.view_loading_anim, null);

                    myticket_listview.addHeaderView(paddingTop);
                    myticket_listview.addFooterView(paddingBottom);
                    myticket_listview.addHeaderView(loadingView);
                    myticket_listview.setAdapter(adapter);
                }
            });

            if(Util.isOnline(getActivity())) {
                ServiceManager s = new ServiceManager(
                        getActivity()
                        , Setting.getDeviceId(getActivity())
                        , Setting.getManufacturer()
                        , Setting.getModel()
                        , Setting.getOsVersion());

                List<Ticket> ticketList = s.getTicket(Setting.getUserID());

                for(Ticket ticket : ticketList) {

                    final com.focalsolution.ticketproject.Adapter.MyTicket.Entity entity = new com.focalsolution.ticketproject.Adapter.MyTicket.Entity();
                    entity.barcodeImageUrl = "http://www.barcodes4.me/barcode/c128a/"+ticket.code+".png?width=600&height=200";
                    entity.coverImage = ticket.the_event.cover_photo.url;
                    entity.date = ticket.the_event.getStartTime("EEEE dd MMMM");
                    entity.year = ticket.the_event.getStartTime("yyyy");
                    entity.hall = "Test hall"; //ticket.ticket_type.name;
                    entity.seat = ticket.ticket_type.name;
                    entity.isUsed = ticket.used;
                    entity.row = "Test Row";
                    entity.id = ticket.id;
                    entity.name = ticket.the_event.name;
                    entity.location = ticket.the_event.location_formatted_address;
                    entity.lat = ticket.the_event.location_lat;
                    entity.lng = ticket.the_event.location_lng;

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.addItem(entity);
                        }
                    });
                }

                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        myticket_listview.removeHeaderView(loadingView);
                    }
                });

            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Cannot connect to Server!", Toast.LENGTH_LONG).show();
                        myticket_listview.removeHeaderView(loadingView);
                    }
                });
            }

        }
    };
}
