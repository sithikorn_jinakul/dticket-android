package com.focalsolution.ticketproject.Fragment;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.focalsolution.ticketproject.Home.HomeActivity;
import com.focalsolution.ticketproject.R;
import com.focalsolution.ticketproject.Setting;
import com.focalsolution.utility.Util;
import com.focalsolution.webservice.Entity.EventList;
import com.focalsolution.webservice.ServiceManager;

import java.util.List;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class HomeFragment extends Fragment {
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    private ListView view_lv_product;
    private View loadingView;
    private com.focalsolution.ticketproject.Adapter.Product.Adapter adapter;
    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static HomeFragment newInstance(int sectionNumber) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public HomeFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);

        setLayout(rootView);

        setEvent();

        //loadData();

        return rootView;
    }

    private void setLayout(View rootView) {
        view_lv_product = (ListView) rootView.findViewById(R.id.view_lv_product);
        adapter = new com.focalsolution.ticketproject.Adapter.Product.Adapter(getActivity());

        loadingView = LayoutInflater.from(getActivity()).inflate(R.layout.view_loading_anim, null);
    }

    private void setEvent() {
        view_lv_product.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                com.focalsolution.ticketproject.Adapter.Product.Entity data = adapter.getItem(position);
                Intent i = new Intent("com.focalsolution.ticketproject.Detail.DetailActivity");
                i.putExtra("event_id", data.id );
                startActivity(i);
            }
        });
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((HomeActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));
    }

    public void onDropDownChange(int position)
    {
        if(position == 0) {
            new Thread(GetDataRunnable).start();
        } else {
            new Thread(GetDataRunnable).start();
        }

        /*
        adapter.clearAllItem();
        view_lv_product.addHeaderView(loadingView);
        view_lv_product.setAdapter(adapter);
        adapter.addItem(new com.focalsolution.ticketproject.Adapter.Product.Entity("“GREEN CONCERT#17\n" +
                "LOVE SCENES LOVE SONGS\n" +
                "CLUB FRIDAY 9th ANNIVERSARY”"
                , "27 - 28 September 2014"
                , 3000
                , "http://www.thaiticketmajor.com/concert/images/green-concert-17-2014/poster-3.jpg"));

        adapter.addItem(new com.focalsolution.ticketproject.Adapter.Product.Entity("bodyslam ปรากฎการณ์ ดัม-มะ-ชา-ติ"
                , "12 October 2014"
                , 1500
                , "http://www.thaiticketmajor.com/concert/images/bodyslam-2014/poster.jpg"));

        adapter.addItem(new com.focalsolution.ticketproject.Adapter.Product.Entity("คอนเสิร์ต ขนนกกับดอกไม้\nTHE ORIGINAL RETURNS"
                , "28 February 2014"
                , 6000
                , "http://www.thaiticketmajor.com/concert/images/bird-khonnok-return-2014/poster-1.jpg"));

        adapter.addItem(new com.focalsolution.ticketproject.Adapter.Product.Entity("Patiparn Party\n25 ปี Mr.Mos"
                , "29 November 2014"
                , 3000
                , "http://www.thaiticketmajor.com/concert/images/patiparn-party-mos-2014/poster.jpg"));

        adapter.notifyDataSetChanged();
        */
    }

    Runnable GetDataRunnable = new Runnable() {
        @Override
        public void run() {

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    adapter.clearAllItem();
                    adapter.notifyDataSetChanged();
                    view_lv_product.addHeaderView(loadingView);
                    view_lv_product.setAdapter(adapter);
                }
            });

            if(Util.isOnline(getActivity()))
            {
                ServiceManager s = new ServiceManager(
                        getActivity()
                        , Setting.getDeviceId(getActivity())
                        , Setting.getManufacturer()
                        , Setting.getModel()
                        , Setting.getOsVersion());

                List<EventList> data = s.getEvent();

                if(data!=null)
                {
                    for (final EventList eventList : data)
                    {
                        String cover = R.drawable.noimage_dark + "";
                        if(eventList.cover_photo != null)
                            cover = eventList.cover_photo.url;

                        final String c = cover;

                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                adapter.addItem(
                                        new com.focalsolution.ticketproject.Adapter.Product.Entity(
                                                eventList.id
                                                , eventList.name
                                                , eventList.getStartTime("EEEE dd MMMM yyyy")
                                                , eventList.price
                                                , c));
                            }
                        });

                    }

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            adapter.notifyDataSetChanged();
                            view_lv_product.removeHeaderView(loadingView);
                        }
                    });
                }

            } else {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getActivity(), "Cannot connect to Server!", Toast.LENGTH_LONG).show();
                        view_lv_product.removeHeaderView(loadingView);
                    }
                });
            }
        }
    };
}
