package com.focalsolution.ticketproject;

import android.content.Context;
import android.os.Build;
import android.os.Environment;
import android.provider.Settings;

import com.focalsolution.utility.Util;

import java.io.File;

/**
 * Created by sithikorn on 9/22/14 AD.
 */
public class Setting {

    private static String getDefaultFolder(Context c)
    {
        return Environment.getExternalStorageDirectory() + "/Android/data/" + c.getApplicationContext().getPackageName();
    }

    public static String getDefaultCacheDir(Context c)
    {
        File f = c.getExternalCacheDir();
        if(f != null) {
            String cachePath = f.getAbsolutePath();
            return cachePath;
        }

        Util.dirChecker(getDefaultFolder(c) + "cache/");
        return getDefaultFolder(c) + "cache/";
    }

    public static String getDeviceId(Context c) {
        return Settings.Secure.getString(c.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String getManufacturer() {
        return Build.MANUFACTURER;
    }

    public static String getModel() {
        return Build.MODEL;
    }

    public static String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    public static int getUserID()
    {
        return 9;
    }
}
